<!-- BEGIN: main -->
<div class="content_header_out_design pd_40">
    <div class="col-xs-24 col-sm-12 col-md-12 col-lg-12 ">
        <div class="txt"><h2><a  href="{LINK}" class="size_60 hover_red" title="{TITLE}">{TITLE}</a></h2></div>
        <a title="{TITLE}" class="a_link"><img alt="{TITLE}" src="{IMAGES}" /></a>
    </div>
    <div class="col-xs-24 col-sm-12 col-md-12 col-lg-12 " style="padding-left:0;">
        <div class="content_about">
            <div class="bodytext">{BODYTEXT}</div>
            <p class="div_red_link">
                <input type="hidden" class="link_title" value="http://beulogo.my:8080/seek/">
                <span class="content_a_link_red">
                <span class="content_icon_left"><i class="fa fa-plus" aria-hidden="true"></i></span>
                    <a href="http://beulogo.my:8080/seek/" title="CLIENTS" target="_blank"><span>GET STARTED</span> <span class="content_icon_right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a> </span>
            </p>
        </div>
    </div>
    
</div>
<!-- END: main -->
