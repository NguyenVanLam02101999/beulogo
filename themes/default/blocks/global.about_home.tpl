<!-- BEGIN: main -->
<div class="wraper row content reorder-xs">
    <div class="content_header">
        <div class="col-xs-24 col-sm-12 col-md-12 rotate" style="padding-left:0;">
            <div class="txt">
                
                <h2><span class="border_logic_header"></span><a  href="{LINK}" class="size_60 hover_red" title="{TITLE}">{TITLE}</a></h2>
            </div>
            <div class="content_about">
                <p class="">{DESCRIPTION}</p>
                <div class="bodytext">
                    {BODYTEXT}
                </div>
            </div>
        </div>
        <div class="col-xs-24 col-sm-12 col-md-12 rotate">
            <a href="{LINK}" title="{TITLE}" class="a_link"><img alt="{TITLE}" src="{IMAGES}" /></a>
        </div>
    </div>
</div>
<!-- END: main -->
