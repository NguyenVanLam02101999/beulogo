<!-- BEGIN: main -->
<div class="frct">
	<div class="panel-body">
		<div class="featured-products about_product">
			<!-- BEGIN: loop -->
			<div class="row">
				<div class="col-xs-24 col-sm-8 col-md-8 col-lg-8">
					<!-- BEGIN: image_only --><img title="{ROW.title}" src="{ROW.image}" class="img-thumbnail"><!-- END: image_only -->
					<!-- BEGIN: image_link -->
					<div class="hidden hide_button">
						<input type="hidden" class="link_title" value="{ROW.link}">
						<span class="content_a_link_red">
							<span class="content_icon_left"><i class="fa fa-plus" aria-hidden="true"></i></span>
							<a href="{ROW.link}" title="{ROW.title}"<!-- BEGIN: target --> target="{ROW.target}"<!-- END: target -->><span>GET STARTED</span> <span class="content_icon_right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </span>
						</span>
					</div> 
					<a href="{ROW.link}" title="{ROW.title}"<!-- BEGIN: target --> target="{ROW.target}"<!-- END: target -->><img src="{ROW.image}" title="{ROW.title}" class="img-thumbnail"></a><!-- END: image_link -->
				</div>
				<div class="col-xs-24 col-sm-16 col-md-16 col-lg-16 content_">
					<div class="center-block_product">
						<p class="title">
							<a class="title_link_a">{ROW.title}</a>
						</p>
						<div class="bodytext">
							{ROW.description}
						</div>
						<div class="div_red_link">

						</div>
					</div>
				</div>
			</div>
			<!-- END: loop -->
		</div>
	</div>
</div>

<!-- END: main -->