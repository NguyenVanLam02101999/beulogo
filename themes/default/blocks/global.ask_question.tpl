<!-- BEGIN: main -->
<div class="as_dv">
	<div class="panel-body">
		<div class="row ask_question">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
		        <h3 class="title_top ">{INFO.title}</h3>
		        <p class="mota_">{INFO.description}</p>
		        <div class="hidden-lg hidden-md hidden-sm">
				    <a class="a_link">
			    	   <img alt="{INFO.title}" src="{INFO.link_image}" class="image_right">
				    </a>
		    	</div>
		        <div class="row_detail">
		        	<!-- BEGIN: loop -->
		        	<div class="row row_info">
						<div class="row_title_info">
							<div class="col-md-20 col-xs-22">
								<p class="title_info">{ROW.title} </p>
							</div>
							<div class="col-md-4 col-xs-2">
								<span class="down_plus"><i class="fa fa-plus" aria-hidden="true"></i></span>
							</div>
							<div class="col-md-24 col-xs-24 down_info">
								{ROW.description}
							</div>
						</div>
		        	</div>
					<!-- END: loop -->
				</div>
		    </div>
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 hidden-xs">
			    <a class="a_link">
		    	   <img alt="{INFO.title}" src="{INFO.link_image}" class="image_right">
			    </a>
		    </div>
			
		</div>
	</div>
</div>
<!-- END: main -->
