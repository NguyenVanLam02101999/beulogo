<!-- BEGIN: main  -->
<div class="header_about">
	<!-- BEGIN: loop -->
	<div class="nv-block-banners">
	    <!-- BEGIN: type_image_link -->
	    <a rel="nofollow" href="{DATA.link}" onclick="this.target='{DATA.target}'" title="{DATA.file_alt}"><img alt="{DATA.file_alt}" src="{DATA.file_image}" width="{DATA.file_width}"></a>
	    <!-- END: type_image_link -->
	    <!-- BEGIN: type_image -->
	    <img alt="{DATA.file_alt}" src="{DATA.file_image}" width="{DATA.file_width}">
	    <!-- END: type_image -->
	    <!-- BEGIN: bannerhtml -->
	    <div class="clearfix text-left">
	        {DATA.bannerhtml}
	    </div>
	    <!-- END: bannerhtml -->
	</div>
	<div class="icon_heder"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
</div>
<!-- END: loop -->
<!-- END: main -->
