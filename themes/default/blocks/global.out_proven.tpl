<!-- BEGIN: main -->
<div class="our_proven_step">
    <div class="col-md-24">
    <div class="txt"><h2><a class="size_60 hover_red" title="{TITLE}">{CONFIG.title_top}</a></h2></div>
    </div>
    <div class="row row_proven">
        <!-- BEGIN: loop -->
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-24">
                <div class="hinhanh text-center">
                    <a href="{VIEW.link}" title="{VIEW.title}" class="hinhanh_how_work_it"><img alt="{TITLE}" src="{VIEW.image}" /></a>
                </div>
                <div class="tieude_proven">
                    <h3>
                        {VIEW.title}
                    </h3>
                </div>
                <div class="mota">
                    {VIEW.bodytext}
                </div>

            </div>
        <!-- END: loop -->
    </div>
</div>
<!-- END: main -->
