<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2015 VINADES ., JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Jan 10, 2011 6:04:30 PM
 */

if (!defined('NV_MAINFILE'))
    die('Stop!!!');

if (!nv_function_exists('nv_block_ask_question')) {
    /**
     * nv_block_config_text_banner()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_ask_question($module, $data_block, $lang_block)
    {
        global $site_mods, $nv_Cache;
        $module = 'freecontent';
        $html = '';

        $link  = NV_BASE_SITEURL . 'assets/js/select2/';
        $js1 = $link.'select2.min.js';
        $js2 = $link.'i18n/vi.js';
        $css = $link.'select2.min.css';
        $html .= '<script type="text/javascript" src="'.$js1.'"></script>';
        $html .= '<script type="text/javascript" src="'.$js2.'"></script>';
        $html .= '<link rel="stylesheet" type="text/css" href="'.$css.'">';
        $html .= '<script type="text/javascript"></script>';

        $html .= '<div class="form-group">';
        $html .= '  <label class="control-label col-sm-6">'. $lang_block['block'].'</label>';

        $html .= '  <div class="col-sm-18">';
        $html .= '      <select name="config_blockid" id="config_blockid" class="form-control">';
        $sql = 'SELECT bid, title FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_blocks ORDER BY title ASC';

        $list = $nv_Cache->db($sql, '', $module);

        foreach ($list as $row) {
            $html .= '  <option value="' . $row['bid'] . '"' . ($row['bid'] == $data_block['blockid'] ? ' selected="selected"' : '') . '>' . $row['title'] . '</option>';
            $block .= nv_get_blocklist_ask_question($module, $row['bid'] );
        }

        $post = '["'.implode('","', $data_block['baiviet']).'"]';

        $html .= '      </select>';
        $html .= '  <div style="display: none">' . $block . '</div>';
        $html .= '  </div>';
        $html .= '</div>';

        $html .= '<script type="text/javascript">';
        $html .= 'id = $("#config_blockid").val();
                h = $("#select_freecontent_"+id).html()
                $(".baiviet").html(h);';
        $html .= '$("#config_blockid").change(function(event) {';
        $html .= '$(".baiviet").html("");';
        $html .= 'id = $(this).val();';
        $html .= 'h = $(this).parent().find("#select_freecontent_"+id).html();';
        $html .= '$(".baiviet").html(h);';
        $html .= '});';
        $html .= '$(".baiviet").select2(); $(".baiviet").select2().val('.$post.').trigger("change")';
       
        $html .= '</script>';

        $html .= '<div class="form-group">';
        $html .= '  <label class="control-label col-sm-6">'. $lang_block['new'].':</label>';
        $html .= '  <div class="col-sm-18">';
        $html .= '      <select name="baiviet[]" class="form-control baiviet"  multiple="multiple">';
        $html .= '      </select>';
        $html .= '  </div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '  <label class="control-label col-sm-6">Chọn ảnh:</label>';
        $html .= '  <div class="col-sm-18">';
        $html .= '<div class="input-group">';
            $html .= '<input class="form-control" type="text" name="config_image" value="'.$data_block['image'].'" required id="image" placeholder="Vui lòng chọn ảnh..." /> <span class="input-group-btn">';
                $html .= '<button class="btn btn-default" onclick="nv_selectfile($(this)); return !1;" data-currentpath="'.NV_UPLOADS_DIR.'" type="button">
                    <em class="fa fa-folder-open-o fa-fix">&nbsp;</em>';
                $html .= '</button>';
            $html .= '</span>';
        $html .= '</div>';
        $html .= '  </div>';
        $html .= '</div>';
        $html .= '<script type="text/javascript">';
        $html .= 'function nv_selectfile($this)
        {
            var area = "image";
            var path = "{NV_UPLOADS_DIR}";
            var currentpath = $this.data("currentpath");
            var type = "image";
            x=  nv_open_browse(script_name + "?" + nv_name_variable
                    + "=upload&popup=1&area=" + area + "&path="
                    + path + "&type=" + type + "&currentpath="
                    + currentpath, "NVImg", 850, 420,
                    "resizable=no,scrollbars=no,toolbar=no,location=no,status=no");
        }';

        $html .= '</script>';
        return $html;
    }

    /**
     * nv_block_config_text_banner_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */

    function nv_get_blocklist_ask_question( $module, $id )
    {
        global $site_mods, $data_block, $nv_Cache, $lang_block;

        $html = '';
        $html .= '<div id="select_' . $module . '_'.$id.'">';
        $html .= '<select  class="form-control baiviet_select2 " >';
        $html .= '<option value="0">---' . $lang_block['new'] . '---</option>';
        $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_rows WHERE bid = '.$id.' ORDER BY id ASC';
        $list = $nv_Cache->db($sql, '', $module);
        foreach ($list as $l) {
            $html .= '<option value="' . $l['id'] . '">' . $l['title'] . '</option>';
        }
        $html .= '</select>';
        $html .= '</div>';

        return $html;
    }

    function nv_block_config_ask_question_submit_block($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['blockid'] = $nv_Request->get_int('config_blockid', 'post', 0);
        $return['config']['image'] = $nv_Request->get_title('config_image', 'post', "");
        $return['config']['baiviet'] = $_POST['baiviet'];
        return $return;
    }


    /**
     * nv_block_global_text_banner()
     *
     * @param mixed $block_config
     * @return
     */
   function nv_block_ask_question($block_config)
    {

        global $global_config, $site_mods, $module_config, $nv_Cache, $db;
        
        $mod_name = 'freecontent';

        
        $module = $mod_name;

        $post = implode(",", $block_config['baiviet']);

        if (!isset($site_mods[$module]) or empty($block_config['blockid'])) {
            return '';
        }

        $sql = 'SELECT id, title, description, image, link, target FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_rows WHERE status = 1 AND bid = ' . $block_config['blockid'] .' AND id IN ('.$post.')';
        $list = $nv_Cache->db($sql, 'id', $module);
        $tb_freecont_block = NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_blocks';
        $sql_freecontent_block = "SELECT * FROM $tb_freecont_block WHERE bid =".$block_config['blockid'];
        $result_freecontent_block = $db->query($sql_freecontent_block)->fetch();
        $result_freecontent_block['link_image'] = $block_config['image'];
        if (!empty($list)) {
            if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.ask_question.tpl')) {
                $block_theme = $global_config['module_theme'];
            } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.ask_question.tpl')) {
                $block_theme = $global_config['site_theme'];
            } else {
                $block_theme = 'default';
            }

            $xtpl = new XTemplate('global.ask_question.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');

            if ($block_config['numrows'] <= sizeof($list)) {
                $list = array_slice($list, 0, $block_config['numrows']);
            }

            foreach ($list as $row) {
                if (!empty($row['image'])) {
                    $row['image'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $row['image'];
                }

                $xtpl->assign('ROW', $row);

                if (!empty($row['link'])) {
                    if (!empty($row['target'])) {
                        $xtpl->parse('main.loop.title_link.target');
                    }

                    $xtpl->parse('main.loop.title_link');
                } else {
                    $xtpl->parse('main.loop.title_text');
                }

                if (!empty($row['image'])) {
                    if (!empty($row['link'])) {
                        if (!empty($row['target'])) {
                            $xtpl->parse('main.loop.image_link.target');
                        }
                        $xtpl->parse('main.loop.image_link');
                        
                    } else {
                        $xtpl->parse('main.loop.image_only');
                    }
                }

                $xtpl->parse('main.loop');
            }
            $xtpl->assign('INFO', $result_freecontent_block);
            $xtpl->parse('main');
            return $xtpl->text('main');
        }

        return '';
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_ask_question($block_config);
}
