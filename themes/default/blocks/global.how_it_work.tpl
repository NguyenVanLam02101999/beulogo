<!-- BEGIN: main -->
<div class="wraper row content_how_it_word pd_40">
	<div class="content_how_it_word">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
            <div class="txt"><h2><a  href="{LINK}" class="size_60 hover_red" title="{TITLE}">{TITLE}</a></h2><span class="border_logic_down"></span></div>
            <div class="hidden-lg hidden-md hidden-sm" >
                <a href="{LINK}" title="{TITLE}" class="a_link"><img alt="{TITLE}" src="{IMAGES}" /></a>
            </div>
            
            <div class="content_about_work_it">
                <p class="moto_des"><b>{DESCRIPTION}</b></p>
                <div class="bodytext">
                    {BODYTEXT}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 hidden-xs ">
            <a href="{LINK}" title="{TITLE}" class="a_link"><img alt="{TITLE}" src="{IMAGES}" /></a>
        </div>
    </div>
</div>
<!-- END: main -->
