/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC <contact@vinades.vn>
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate 31/05/2010, 00:36
 */

/* Các tùy chỉnh JS của giao diện nên để vào đây */
$(document).ready(function($) {
	daucham = "<div class='row'>" + $(".daucham").html() + "</div>";
	row =  parseInt($(".height_row").val());
	$(".fa-chevron-down").click(function(event) {
	   $('html, body').animate({
            scrollTop: $(".border_logic_header").offset().top - 70
        }, 1000);
	});
	$(".section-body .about_product img").after("<div class='lople'></div>");
	demRow = $(".section-body .about_product .row").length;
	for(i = 1; i <= demRow ; i++){
		if(i %2 ==0){
			$(".section-body .about_product .row").eq(i-1).find(".col-md-8").attr("style", "float:right");
			$(".section-body .about_product .row").eq(i-1).find(".col-md-16").attr("style", "float:left");
			$(".section-body .about_product .row").eq(i-1).find(".lople").addClass('lopchan');
			$(".section-body .about_product .row").eq(i-1).find(".lople").removeClass("lople");
			
		}else{
			$(".section-body .about_product .row").eq(i-1).find(".col-md-8").attr("style", "float:left");
			$(".section-body .about_product .row").eq(i-1).find(".col-md-16").attr("style", "float:right");
		}
		if(i %3 == 0){
			$(".section-body .about_product .row").eq(i-1).find(".lople").remove();
		}
		if( i >= 2 && i%2==0 ){
			$(".section-body .about_product .row").eq(i-1).find(".center-block_product").attr("style", "padding-left:0;");
		}
		if(i >=3 && i%3==0){
			link_button = $(".section-body .about_product .row").eq(i-1).find(".hide_button");
			$(".section-body .about_product .row").eq(i-1).find(".div_red_link").html(link_button.html());
			href = $(".section-body .about_product .row").eq(i-1).find(".link_title").val();
			$(".section-body .about_product .row").eq(i-1).find(".title_link_a").attr('href', href);
			$(".section-body .about_product .row").eq(i-1).find(".title_link_a").addClass('hover_red');
		}
	}
	// $(".hienthibenduoi").hide();
	// $(".content_icon_left").click(function(event) {
	// 	event.preventDefault();
	// 	// if the menu is visible slide it up
	//     $(this).parent().parent().find(".content_icon_left").html('<i class="fa fa-minus" aria-hidden="true"></i>');
	//     if ($(this).parent().parent().parent().find(".hienthibenduoi").is(":visible"))
	//     {
	//         $(this).parent().parent().parent().find(".hienthibenduoi").slideUp(1000);
	//         $(this).parent().parent().find(".content_icon_left").html('<i class="fa fa-plus" aria-hidden="true"></i>');
	//     }
	//     // otherwise, slide the menu down
	//     else
	//     {
	//         $(this).parent().parent().parent().find(".hienthibenduoi").slideDown(1000);
	//         $(this).parent().parent().find(".content_icon_left").html('<i class="fa fa-minus" aria-hidden="true"></i>');

	//     }
	// 	// $(this).parent().parent().parent().find(".hienthibenduoi").slideDown(1000);
	// });

	$(".down_info").hide();
	// $(".hienthibenduoi").hide();
	$(".down_plus").click(function(event) {
		event.preventDefault();
		// if the menu is visible slide it up

		if (!$(this).parent().parent().parent().find(".down_info").is(":visible"))
	    {
	       $(this).parent().parent().parent().find(".down_info").slideDown('slow');
	       $(this).parent().parent().parent().find(".down_plus").html('<i class="fa fa-times" aria-hidden="true"></i>');
	    }
	    // otherwise, slide the menu down
	    else
	    {
	       $(this).parent().parent().parent().find(".down_info").slideUp('slow');
	       $(this).parent().parent().parent().find(".down_plus").html('<i class="fa fa-plus" aria-hidden="true"></i>');
	    }
	    
	   
	});

});