# Các tài nguyên dành cho lập trình module, blocks, giao diện

## Hướng dẫn chuyển đổi font sang web-font

1. Vô https://fonts.google.com/ tìm lấy 1 font ưng ý và tải về. Hiện tại những font sau dùng rất nhiều

- https://fonts.google.com/specimen/Roboto?query=roboto => Dùng làm font toàn site
- https://fonts.google.com/specimen/Open+Sans?query=open+ => Dùng làm font toàn site
- https://fonts.google.com/specimen/Roboto+Condensed?query=robo => Font cho tiêu đề

> Chú ý tìm những font có hỗ trợ tiếng Việt.

2. Giải nén file zip về được các font đuôi .ttf
3. Vô trang https://transfonter.org/ click Add fonts rồi chọn tất cả các font đã giải nén được sau đó ấn Convert, xong thì download. Chú ý đừng thay đổi mấy tùy chọn bên dưới.
4. Giải nén file zip tải về được vào thư mục themes/ten-themes/fonts
5. Xóa file demo.html trong thư mục fonts
6. Mở file stylesheet.css copy nội dung của nó rồi xóa nó
7. Mở file style.css của giao diện dán nội dung trong stylesheet.css vừa copy.
8. Sửa đoạn code đã dán lại cho đúng đường dẫn ví dụng

```
@font-face {
    font-family: 'Open Sans';
    src: url('OpenSans-BoldItalic.woff2') format('woff2'),
        url('OpenSans-BoldItalic.woff') format('woff');
    font-weight: bold;
    font-style: italic;
    font-display: swap;
}
```

Thành

```
@font-face {
    font-family: 'Open Sans';
    src: url('../fonts/OpenSans-BoldItalic.woff2') format('woff2'),
        url('../fonts/OpenSans-BoldItalic.woff') format('woff');
    font-weight: bold;
    font-style: italic;
    font-display: swap;
}
```

9. Thay lại font family cần thiết

## Giao diện

- Font đọc báo https://fonts.google.com/specimen/Noto+Serif?query=noto
- Hiệu ứng background chạy theo khi cuộn Parallax Backgrounds http://pixelcog.github.io/parallax.js/
- Icon mảnh, đẹp để thay FontAwesome https://themify.me/themify-icons
- Icon mảnh, free 170 icons https://linearicons.com/free
- Icon mảnh, nhiều https://github.com/ionic-team/ionicons giấy phép MIT

- Vệt bo (mờ hai đầu) sử dụng 

```
    background-image: -o-linear-gradient(left center, transparent, rgba(0, 0, 0, 0.2), transparent);
    background-image: -webkit-gradient(linear, left center, transparent, rgba(0, 0, 0, 0.2), transparent);
    background-image: -ms-linear-gradient(left center, transparent, rgba(0, 0, 0, 0.2), transparent);
    background-image: -moz-linear-gradient(left center, transparent, rgba(0, 0, 0, 0.2), transparent);
    background-image: -webkit-linear-gradient(left center, transparent, rgba(0, 0, 0, 0.2), transparent);
    background-image: linear-gradient(to left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0));
```

- Ảnh fix dạng cover

```
img {
    object-position: center;
    object-fit: cover;
    width: 100%;
    height: 100%;
}
```

- Thanh cuộn đẹp (không còn phát triển, tích hợp nhanh): https://github.com/gromo/jquery.scrollbar, lưu ý cần có max-height trong css, overflow: auto trong CSS chứ không được trong thẻ style
- Thanh cuộn đẹp hiện đại hơn https://github.com/mdbootstrap/perfect-scrollbar
- Hover vô thì zoom lên dùng CSS transform: scale(1.2); thẻ bên ngoài nó overflow hidden, nhớ dùng transition: all .2s ease-in-out;
- Zoom len ảnh sản phẩm https://github.com/igorlino/elevatezoom-plus

## Block

> Các block đặt trong thư mục blocks. Copy chúng vào themes/ten-theme/blocks để phát triển. Mỗi block có 3 file

- Block shops cat, chạy trên module shops, không hỗ trợ chạy trên module khác global.home_shops.php
- Block news từ nhóm tin global.home_news_center.php
- Block news tin mới, ngoại trừ các chuyên mục global.news_center.php
- Block news lấy từ chuyên mục global.home_student.php
- Block có nút duyệt ảnh global.about_intro.php
- Block có trình soạn thảo global.about_intro.php
- Block slider logo global.sliders_logo.php. Thư mục plugins đặt ngang hàng file theme.php của giao diện
- Block jssor slider lấy từ quảng cáo có arrow svg đẹp global.sliders.php. File jssor.slider.css đặt vào thư mục css, file jssor.slider-21.1.5.mini.js đặt vào thư mục js
- Block giao diện lấy tin dừ videoclips dạng slider global.video_slider.php
- Block menu xuất ra dạng ul 1 cấp đơn giản global.simple_menu.php => Thích hợp làm footer menu
- Block menu chính của site global.menu.php. slimmenu.css đặt vào thư mục css. jquery.slimmenu đặt vào thư mục js
- Block có cấu hình, nhập input text global.info_ussh.php
- Block list đính kèm tuần của lịch công tác workcalendar global.workcalendar_attach.php
