<!-- BEGIN: main -->
<div class="slider-video-wraper">
    <div class="owl-carousel" id="owl-carousel-{CONFIG.bid}">
        <!-- BEGIN: loop -->
        <div class="slider-video-item">
            <div class="thumb">
                <a class="img" href="{ROW.link}" style="background-image: url('{ROW.img}');"><span>{ROW.title}</span></a>
            </div>
            <div class="meta">
                <i class="fa fa-eye" aria-hidden="true"></i> {ROW.view}
            </div>
            <h3><a href="{ROW.link}">{ROW.title}</a></h3>
        </div>
        <!-- END: loop -->
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    function controlCarouselIcon() {
        var element = $('#owl-carousel-{CONFIG.bid}');
        var thumbItem = $('.thumb:first', element);
        var iconTop = (thumbItem.height() - 85) / 2;
        $('.owl-prev,.owl-next', element).css({
            top: iconTop + 'px'
        });
    };
    var element = $('#owl-carousel-{CONFIG.bid}');
    var owl = element.owlCarousel({
        margin: 10,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3
            },
            768: {
                items: 4
            },
            992: {
                items: 5
            }
        },
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        onChanged: function(event) {
            setTimeout(function() {
                controlCarouselIcon();
            }, 50);
        },
        onInitialize: function(event) {
            setTimeout(function() {
                controlCarouselIcon();
            }, 50);
        }
    });
});
</script>
<!-- END: main -->
