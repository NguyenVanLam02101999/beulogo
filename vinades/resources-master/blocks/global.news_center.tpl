<!-- BEGIN: main -->
<link rel="stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/OwlCarousel2/assets/owl.carousel.min.css">
<link rel="stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/OwlCarousel2/assets/owl.theme.default.min.css">
<script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/OwlCarousel2/owl.carousel.min.js"></script>
<div class="owl-carousel carousel-homenews" id="OwlCarousel-b{CONFIG.bid}">
    <!-- BEGIN: loop -->
    <div class="item">
        <div class="img animate-thumb">
            <a href="{ROW.link}"{ROW.target_blank} style="background-image: url('{ROW.imgsourcebig}');"><img src="{ROW.imgsourcebig}" alt="{ROW.title}"></a>
        </div>
        <h3 class="mb-2"><a href="{ROW.link}"{ROW.target_blank}>{ROW.title}</a></h3>
        <div class="t text-muted mb-2">{ROW.publtime}</div>
        <div class="htext">{ROW.hometext}</div>
    </div>
    <!-- END: loop -->
</div>
<script>
$(document).ready(function(){
    $("#OwlCarousel-b{CONFIG.bid}").owlCarousel({
        margin: 30,
        loop: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        },
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });
});
</script>
<!-- END: main -->
