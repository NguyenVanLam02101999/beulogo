<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Sat, 10 Dec 2011 06:46:54 GMT
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (!nv_function_exists('nv_block_video_slider')) {
    /**
     * @param string $module
     * @param array $data_block
     * @param array $lang_block
     * @return string
     */
    function nv_block_config_video_slider($module, $data_block, $lang_block)
    {
        global $nv_Cache, $site_mods, $nv_Request;

        // Xuất nội dung khi có chọn module
        if ($nv_Request->isset_request('loadajaxdata', 'get')) {
            $module = $nv_Request->get_title('loadajaxdata', 'get', '');

            $html = '';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['numrow'] . ':</label>';
            $html .= '<div class="col-sm-9"><input type="text" class="form-control" name="config_numrow" value="' . $data_block['numrow'] . '"/></div>';
            $html .= '</div>';

            nv_htmlOutput($html);
        }

        $html = '';
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['selectmod'] . ':</label>';
        $html .= '<div class="col-sm-9">';
        $html .= '<select name="config_selectmod" class="form-control">';
        $html .= '<option value="">--</option>';

        foreach ($site_mods as $title => $mod) {
            if ($mod['module_file'] == 'videoclips') {
                $html .= '<option value="' . $title . '"' . ($title == $data_block['selectmod'] ? ' selected="selected"' : '') . '>' . $mod['custom_title'] . '</option>';
            }
        }

        $html .= '</select>';

        $html .= '
        <script type="text/javascript">
        $(\'[name="config_selectmod"]\').change(function() {
            var mod = $(this).val();
            var file_name = $("select[name=file_name]").val();
            var module_type = $("select[name=module_type]").val();
            var blok_file_name = "";
            if (file_name != "") {
                var arr_file = file_name.split("|");
                if (parseInt(arr_file[1]) == 1) {
                    blok_file_name = arr_file[0];
                }
            }
            if (mod != "") {
                $.get(script_name + "?" + nv_name_variable + "=" + nv_module_name + \'&\' + nv_lang_variable + "=" + nv_lang_data + "&" + nv_fc_variable + "=block_config&bid=" + bid + "&module=" + module_type + "&selectthemes=" + selectthemes + "&file_name=" + blok_file_name + "&loadajaxdata=" + mod + "&nocache=" + new Date().getTime(), function(theResponse) {
                    $("#block_config").append(theResponse);
                });
            }
        });
        $(function() {
            $(\'[name="config_selectmod"]\').change();
        });
        </script>
        ';

        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    function nv_block_config_video_slider_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['selectmod'] = $nv_Request->get_title('config_selectmod', 'post', '');
        $return['config']['numrow'] = $nv_Request->get_int('config_numrow', 'post', 0);
        return $return;
    }

    function nv_block_video_slider($block_config)
    {
        global $site_mods;

        $block_config['module'] = $block_config['selectmod'];
        $module = $block_config['module'];

        if (isset($site_mods[$module])) {
            global $module_name, $db, $global_config, $nv_Cache;

            $module_info = $site_mods[$module];
            $module_data = $site_mods[$module]['module_data'];
            $module_upload = $site_mods[$module]['module_upload'];

            if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.video_slider.tpl')) {
                $block_theme = $global_config['module_theme'];
            } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.video_slider.tpl')) {
                $block_theme = $global_config['site_theme'];
            } else {
                $block_theme = 'default';
            }

            //include NV_ROOTDIR . '/themes/' . $block_theme . '/language/' . NV_LANG_INTERFACE . '.php';

            $xtpl = new XTemplate('global.video_slider.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
            $xtpl->assign('NV_BASE_SITEURL', NV_BASE_SITEURL);
            $xtpl->assign('TEMPLATE', $block_theme);
            $xtpl->assign('CONFIG', $block_config);
            $xtpl->assign('MODULE_NAME', $module_info['custom_title']);
            $xtpl->assign('MODULE_LINK', NV_BASE_SITEURL . "index.php?" . NV_LANG_VARIABLE . "=" . NV_LANG_DATA . "&amp;" . NV_NAME_VARIABLE . "=" . $module);
            //$xtpl->assign('LANG', $lang_module);

            $sql = "SELECT tb1.*, tb2.view FROM " . NV_PREFIXLANG . "_" . $module_data . "_clip tb1,
            " . NV_PREFIXLANG . "_" . $module_data . "_hit tb2
            WHERE tb1.status=1 AND tb1.id=tb2.cid ORDER BY tb1.id DESC LIMIT " . $block_config['numrow'];
            $array = $nv_Cache->db($sql, '', $module);

            foreach ($array as $row) {
                $row['link'] = NV_BASE_SITEURL . "index.php?" . NV_LANG_VARIABLE . "=" . NV_LANG_DATA . "&amp;" . NV_NAME_VARIABLE . "=" . $module . "&amp;" . NV_OP_VARIABLE . "=video-" . $row['alias'] . $global_config['rewrite_exturl'];
                if (!empty($row['img'] and file_exists(NV_ROOTDIR . '/' . NV_FILES_DIR . '/' . $module_upload . '/' . $row['img']))) {
                    $row['img'] = NV_BASE_SITEURL . NV_FILES_DIR . '/' . $module_upload . '/' . $row['img'];
                } elseif (!empty($row['img'] and file_exists(NV_ROOTDIR . '/' . NV_UPLOADS_DIR . '/' . $module_upload . '/' . $row['img']))) {
                    $row['img'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $module_upload . '/' . $row['img'];
                } else {
                    $row['img'] = NV_BASE_SITEURL . "themes/" . $block_theme . "/images/video.png";
                }

                if (preg_match("/^(http(s)?\:)?\/\/([w]{3})?\.youtube[^\/]+\/watch\?v\=([^\&]+)\&?(.*?)$/is", $row['externalpath'], $m)) {
                    $row['play_link'] = '//www.youtube.com/embed/' . $m[4] . '?rel=0&amp;controls=1&amp;autoplay=1';
                    $row['play_mode'] = 'youtube';
                } elseif (preg_match("/(http(s)?\:)?\/\/youtu?\.be[^\/]?\/([^\&]+)$/isU", $row['externalpath'], $m)) {
                    $row['play_link'] = '//www.youtube.com/embed/' . $m[3] . '?rel=0&amp;controls=1&amp;autoplay=1';
                    $row['play_mode'] = 'youtube';
                } else {
                    $row['filepath'] = !empty($row['internalpath']) ? NV_BASE_SITEURL . $row['internalpath'] : $row['externalpath'];
                    $row['play_link'] = $row['filepath'];
                    $row['play_mode'] = 'player';
                }
                $row['view'] = number_format($row['view'], 0, ',', '.');

                $xtpl->assign('ROW', $row);
                $xtpl->parse('main.loop');
            }

            $xtpl->parse('main');
            return $xtpl->text('main');
        }
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_video_slider($block_config);
}
