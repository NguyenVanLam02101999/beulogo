<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2015 VINADES ., JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Jan 10, 2011 6:04:30 PM
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (!nv_function_exists('nv_block_global_info_ussh')) {
    function nv_block_data_config_info_ussh($module, $data_block, $lang_block)
    {
        $html = '';
        $html .= '<div class="form-group">';

        $html .= "<label class=\"control-label col-sm-6\">";
        $html .= $lang_block['disc'];
        $html .= "</label>";
        $html .= "<div class=\"col-sm-18\">";
        $html .= "<input type=\"text\" class=\"form-control\" name=\"config_disc\" value=\"" . $data_block['disc'] . "\"/>";
        $html .= "</div>";
        $html .= "</div>";

        for ($i = 1; $i <= 4; $i++) {
            if (isset($data_block[$i]) or !is_array($data_block[$i])) {
                $data_block[$i] = [
                    'value' => '',
                    'title' => ''
                ];
            }

            $html .= '<div class="form-group">';
            $html .= "<div class=\"col-sm-6\">";
            $html .= "<input type=\"text\" class=\"form-control\" name=\"config_title" . $i . "\" value=\"" . $data_block[$i]['title'] . "\"/>";
            $html .= "</div>";
            $html .= "<div class=\"col-sm-18\">";
            $html .= "<input type=\"number\" class=\"form-control\" name=\"config_value" . $i . "\" value=\"" . $data_block[$i]['value'] . "\"/>";
            $html .= "</div>";
            $html .= "</div>";
        }

        return $html;
    }

    function nv_block_data_config_info_ussh_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['disc'] = $nv_Request->get_title('config_disc', 'post', '');
        for ($i = 1; $i <= 4; $i++) {
            $return['config'][$i]['title'] = $nv_Request->get_title('config_title' . $i, 'post', '');
            $return['config'][$i]['value'] = $nv_Request->get_int('config_value' . $i, 'post', 0);
        }

        return $return;
    }

    function nv_block_global_info_ussh($block_config)
    {
        global $global_config, $client_info, $my_head, $lang_block;
        if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.info_ussh.tpl')) {
            $block_theme = $global_config['module_theme'];
        } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.info_ussh.tpl')) {
            $block_theme = $global_config['site_theme'];
        } else {
            $block_theme = 'default';
        }
        $xtpl = new XTemplate('global.info_ussh.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
        $xtpl->assign('NV_BASE_SITEURL', NV_BASE_SITEURL);
        $xtpl->assign('TEMPLATE', $block_theme);
        $xtpl->assign('LANG', $lang_block);
        $num = 0;
        for ($i = 1; $i <= 4; $i++) {
            $xtpl->assign('title', $block_config[$i]['title']);
            $xtpl->assign('value', $block_config[$i]['value']);
            if (($block_config[$i]['title'] == "") or ($block_config[$i]['value'] == 0)) {
                continue;
            }

            $num += 1;
            $xtpl->parse('main.column');
        }

        $xtpl->assign('COL', 24 / $num);
        $xtpl->assign('BLOCK_CONFIG', $block_config);
        $xtpl->parse('main');
        return $xtpl->text('main');
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_global_info_ussh($block_config);
}
