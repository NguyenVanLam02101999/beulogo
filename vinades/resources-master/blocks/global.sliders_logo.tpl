<!-- BEGIN: main  -->
<link rel="stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/plugins/OwlCarousel2/assets/owl.carousel.min.css">
<link rel="stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/plugins/OwlCarousel2/assets/owl.theme.default.min.css">
<script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/plugins/OwlCarousel2/owl.carousel.min.js"></script>

<div class="owl-slide-partner">
    <div class="owl-carousel" id="owl-slide-partner-{BID}">
        <!-- BEGIN: loop -->
        <div class="s-item">
            <div class="s-item-inner">
                <!-- BEGIN: type_swf -->
                <!-- END: type_swf -->
                <!-- BEGIN: type_image_link -->
                <a href="{DATA.link}" onclick="this.target='{DATA.target}'" title="{DATA.file_alt}"> <img alt="{DATA.file_alt}" src="{DATA.file_image}"> </a>
                <!-- END: type_image_link -->
                <!-- BEGIN: type_image -->
                <img alt="{DATA.file_alt}" src="{DATA.file_image}">
                <!-- END: type_image -->
            </div>
        </div>
        <!-- END: loop -->
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $("#owl-slide-partner-{BID}").owlCarousel({
        items: 7,
        margin: 0,
        loop: true,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        responsive: {
            0 : {items: 1},
            480 : {items: 2},
            768 : {items: 4},
            992 : {items: 5},
            1024 : {items: 7}
        }
    });
});
</script>

<!-- END: main -->
