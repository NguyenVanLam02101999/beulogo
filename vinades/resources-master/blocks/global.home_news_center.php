<?php

use function Composer\Autoload\includeFile;

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate 3/9/2010 23:25
 */

if (! defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (! nv_function_exists('nv_block_home_news_center')) {
    /**
     * nv_block_config_home_news_center()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_home_news_center($module, $data_block, $lang_block)
    {
        global $nv_Cache, $site_mods, $nv_Request;

        // Xuất nội dung khi có chọn module
        if ($nv_Request->isset_request('loadajaxdata', 'get')) {
            $module = $nv_Request->get_title('loadajaxdata', 'get', '');

            $tooltip_position = array(
                'top' => $lang_block['tooltip_position_top'],
                'bottom' => $lang_block['tooltip_position_bottom'],
                'left' => $lang_block['tooltip_position_left'],
                'right' => $lang_block['tooltip_position_right']
            );
            $html_input = '';

            $html = '';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['blockid'] . ':</label>';
            $html .= '<div class="col-sm-9"><select name="config_blockid" class="form-control">';
            $html .= '<option value="0"> -- </option>';
            $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_block_cat ORDER BY weight ASC';
            $list = $nv_Cache->db($sql, '', $module);
            foreach ($list as $l) {
                $html_input .= '<input type="hidden" id="config_blockid_' . $l['bid'] . '" value="' . NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $site_mods[$module]['alias']['groups'] . '/' . $l['alias'] . '" />';
                $html .= '<option value="' . $l['bid'] . '" ' . (($data_block['blockid'] == $l['bid']) ? ' selected="selected"' : '') . '>' . $l['title'] . '</option>';
            }
            $html .= '</select>';
            $html .= $html_input;
            $html .= '<script type="text/javascript">';
            $html .= '	$("select[name=config_blockid]").change(function() {';
            $html .= '		$("input[name=title]").val($("select[name=config_blockid] option:selected").text());';
            $html .= '		$("input[name=link]").val($("#config_blockid_" + $("select[name=config_blockid]").val()).val());';
            $html .= '	});';
            $html .= '</script>';
            $html .= '</div></div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['title_length'] . ':</label>';
            $html .= '<div class="col-sm-18"><input type="text" class="form-control" name="config_title_length" size="5" value="' . $data_block['title_length'] . '"/></div>';
            $html .= '</div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['numrow'] . ':</label>';
            $html .= '<div class="col-sm-18"><input type="text" class="form-control" name="config_numrow" size="5" value="' . $data_block['numrow'] . '"/></div>';
            $html .= '</div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['showtooltip'] . ':</label>';
            $html .= '<div class="col-sm-18">';
            $html .= '<div class="row">';
            $html .= '<div class="col-sm-4">';
            $html .= '<div class="checkbox"><label><input type="checkbox" value="1" name="config_showtooltip" ' . ($data_block['showtooltip'] == 1 ? 'checked="checked"' : '') . ' /></label>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-sm-10">';
            $html .= '<div class="input-group margin-bottom-sm">';
            $html .= '<div class="input-group-addon">' . $lang_block['tooltip_position'] . '</div>';
            $html .= '<select name="config_tooltip_position" class="form-control">';

            foreach ($tooltip_position as $key => $value) {
                $html .= '<option value="' . $key . '" ' . ($data_block['tooltip_position'] == $key ? 'selected="selected"' : '') . '>' . $value . '</option>';
            }

            $html .= '</select>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-sm-10">';
            $html .= '<div class="input-group">';
            $html .= '<div class="input-group-addon">' . $lang_block['tooltip_length'] . '</div>';
            $html .= '<input type="text" class="form-control" name="config_tooltip_length" value="' . $data_block['tooltip_length'] . '"/>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';

            nv_htmlOutput($html);
        }

        $html = '';
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['selectmod'] . ':</label>';
        $html .= '<div class="col-sm-9">';
        $html .= '<select name="config_selectmod" class="form-control">';
        $html .= '<option value="">--</option>';

        foreach ($site_mods as $title => $mod) {
            if ($mod['module_file'] == 'news') {
                $html .= '<option value="' . $title . '"' . ($title == $data_block['selectmod'] ? ' selected="selected"' : '') . '>' . $mod['custom_title'] . '</option>';
            }
        }

        $html .= '</select>';

        $html .= '
        <script type="text/javascript">
        $(\'[name="config_selectmod"]\').change(function() {
            var mod = $(this).val();
            var file_name = $("select[name=file_name]").val();
            var module_type = $("select[name=module_type]").val();
            var blok_file_name = "";
            if (file_name != "") {
                var arr_file = file_name.split("|");
                if (parseInt(arr_file[1]) == 1) {
                    blok_file_name = arr_file[0];
                }
            }
            if (mod != "") {
                $.get(script_name + "?" + nv_name_variable + "=" + nv_module_name + \'&\' + nv_lang_variable + "=" + nv_lang_data + "&" + nv_fc_variable + "=block_config&bid=" + bid + "&module=" + module_type + "&selectthemes=" + selectthemes + "&file_name=" + blok_file_name + "&loadajaxdata=" + mod + "&nocache=" + new Date().getTime(), function(theResponse) {
                    $("#block_config").append(theResponse);
                });
            }
        });
        $(function() {
            $(\'[name="config_selectmod"]\').change();
        });
        </script>
        ';

        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * nv_block_config_home_news_center_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_home_news_center_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['selectmod'] = $nv_Request->get_title('config_selectmod', 'post', '');
        $return['config']['blockid'] = $nv_Request->get_int('config_blockid', 'post', 0);
        $return['config']['numrow'] = $nv_Request->get_int('config_numrow', 'post', 0);
        $return['config']['title_length'] = $nv_Request->get_int('config_title_length', 'post', 20);
        $return['config']['showtooltip'] = $nv_Request->get_int('config_showtooltip', 'post', 0);
        $return['config']['tooltip_position'] = $nv_Request->get_string('config_tooltip_position', 'post', 0);
        $return['config']['tooltip_length'] = $nv_Request->get_string('config_tooltip_length', 'post', 0);
        return $return;
    }

    /**
     * nv_block_home_news_center()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_home_news_center($block_config)
    {
        global $site_mods;

        $block_config['module'] = $block_config['selectmod'];
        $module = $block_config['module'];

        if (isset($site_mods[$module])) {
            global $global_array_cat, $module_name, $db, $global_config, $nv_Cache, $module_config;

            $module_array_cat = array();
            $module_data = $site_mods[$module]['module_data'];
            $module_upload = $site_mods[$module]['module_upload'];

            // Xác định danh sách chuyên mục
            if ($module_name == $module) {
                $module_array_cat = $global_array_cat;
            } else {
                $sql = 'SELECT catid, parentid, title, alias, viewcat, subcatid, numlinks, description, status, keywords, groups_view FROM
                ' . NV_PREFIXLANG . '_' . $module_data . '_cat WHERE status=1 OR status=2 ORDER BY sort ASC';
                $list = $nv_Cache->db($sql, 'catid', $module);
                if (!empty($list)) {
                    foreach ($list as $l) {
                        $module_array_cat[$l['catid']] = $l;
                        $module_array_cat[$l['catid']]['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $l['alias'];
                    }
                }
            }

            // Xác định cái group đó
            $sql = "SELECT * FROM " . NV_PREFIXLANG . '_' . $module_data . "_block_cat WHERE bid=" . $block_config['blockid'];
            $array_group = $nv_Cache->db($sql, '', $module);
            if (empty($array_group)) {
                return '';
            }
            $array_group = $array_group[0];

            $db->sqlreset()
            ->select('t1.id, t1.catid, t1.title, t1.alias, t1.homeimgfile, t1.homeimgthumb,t1.hometext,t1.publtime,t1.external_link')
            ->from(NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_rows t1')
            ->join('INNER JOIN ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_block t2 ON t1.id = t2.id')
            ->where('t2.bid= ' . $block_config['blockid'] . ' AND t1.status= 1')
            ->order('t2.weight ASC')
            ->limit($block_config['numrow']);
            $list = $nv_Cache->db($db->sql(), '', $module);

            if (!empty($list)) {
                if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.home_news_center.tpl')) {
                    $block_theme = $global_config['module_theme'];
                } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.home_news_center.tpl')) {
                    $block_theme = $global_config['site_theme'];
                } else {
                    $block_theme = 'default';
                }

                include NV_ROOTDIR . '/themes/' . $block_theme . '/language/' . NV_LANG_INTERFACE . '.php';

                $xtpl = new XTemplate('global.home_news_center.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
                $xtpl->assign('TEMPLATE', $block_theme);
                $xtpl->assign('CONFIG', $block_config);
                $xtpl->assign('LANG', $lang_module);
                $xtpl->assign('LINK_MODULE', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module);
                $xtpl->assign('LINK_MORE', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $site_mods[$module]['alias']['groups'] . '/' . $array_group['alias']);

                foreach ($list as $row) {
                    $row['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $module_array_cat[$row['catid']]['alias'] . '/' . $row['alias'] . '-' . $row['id'] . $global_config['rewrite_exturl'];

                    if ($row['external_link']) {
                        $row['target_blank'] = ' target="_blank"';
                    }
                    $row['publtime'] = nv_date('d/m/Y H:i', $row['publtime']);
                    $row['hometext_clean'] = nv_clean60(strip_tags($row['hometext']), $block_config['tooltip_length']);
                    $row['title_trim'] = nv_clean60($row['title'], $block_config['title_length']);

                    // Ảnh thumb
                    if ($row['homeimgthumb'] == 1) {
                        $row['imgsource'] = NV_BASE_SITEURL . NV_FILES_DIR . '/' . $module_upload . '/' . $row['homeimgfile'];
                    } elseif ($row['homeimgthumb'] == 2) {
                        $row['imgsource'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $module_upload . '/' . $row['homeimgfile'];
                    } elseif ($row['homeimgthumb'] == 3) {
                        $row['imgsource'] = $row['homeimgfile'];
                    } elseif (! empty($module_config[$module]['show_no_image'])) {
                        $row['imgsource'] = NV_BASE_SITEURL . $module_config[$module]['show_no_image'];
                    } else {
                        $row['imgsource'] = NV_BASE_SITEURL . 'themes/' . $global_config['site_theme'] . '/images/no_image.gif';
                    }

                    // Ảnh lớn
                    if (!empty($row['homeimgfile']) and file_exists(NV_UPLOADS_REAL_DIR . '/' . $module_upload . '/' . $row['homeimgfile'])) {
                        $row['imgfull'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $module_upload . '/' . $row['homeimgfile'];
                    } elseif (nv_is_url($row['homeimgfile'])) {
                        $row['imgfull'] = $row['homeimgfile'];
                    } else {
                        $row['imgfull'] = NV_BASE_SITEURL . 'themes/' . $global_config['site_theme'] . '/images/no_image.gif';
                    }

                    $xtpl->assign('ROW', $row);

                    if (!$block_config['showtooltip']) {
                        $xtpl->assign('TITLE', ' title="' . $row['title'] . '"');
                    } else {
                        $xtpl->assign('TITLE', '');
                    }

                    $xtpl->parse('main.loop');
                }

                if ($block_config['showtooltip']) {
                    $xtpl->assign('TOOLTIP_POSITION', $block_config['tooltip_position']);
                    $xtpl->parse('main.tooltip');
                }

                $xtpl->parse('main');
                return $xtpl->text('main');
            }
        }
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_home_news_center($block_config);
}
