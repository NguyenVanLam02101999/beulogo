<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate 3/9/2010 23:25
 */

if (! defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (! nv_function_exists('nv_block_home_shops')) {
    /**
     * nv_block_config_home_shops()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_home_shops($module, $data_block, $lang_block)
    {
        global $nv_Cache, $site_mods, $nv_Request, $db, $language_array, $db_config;

        $module = 'shops';

        // Lấy và chọn danh mục sản phẩm
        $html = '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['catid'] . ':</label>';
        $sql = 'SELECT catid, ' . NV_LANG_DATA . '_title title FROM ' . $db_config['prefix'] . '_' . $site_mods[$module]['module_data'] . '_catalogs WHERE inhome=1 AND parentid=0 ORDER BY sort ASC';
        $list = $nv_Cache->db($sql, '', $module);
        $html .= '<div class="col-sm-18">';
        $html .= '<select class="form-control" name="config_catid">';
        foreach ($list as $l) {
            $html .= '<option value="' . $l['catid'] . '"' . ($l['catid'] == $data_block['catid'] ? ' selected="selected"' : '') . '>' . $l['title'] . '</option>';
        }
        $html .= '</select>';
        $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '	<label class="control-label col-sm-6">' . $lang_block['numrow'] . ':</label>';
        $html .= '	<div class="col-sm-9"><input type="text" name="config_numrow" class="form-control" value="' . $data_block['numrow'] . '"/></div>';
        $html .= '</div>';

        // Lấy hết các bộ menu
        $sql = "SELECT * FROM " . NV_PREFIXLANG . "_menu ORDER BY id DESC";
        $list_menus = $nv_Cache->db($sql, 'id', 'menu');

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['menuid'] . ':</label>';
        $html .= '<div class="col-sm-18">';
        $html .= '<select class="form-control" name="config_menuid">';
        foreach ($list_menus as $l) {
            $html .= '<option value="' . $l['id'] . '"' . ($l['id'] == $data_block['menuid'] ? ' selected="selected"' : '') . '>' . $l['title'] . '</option>';
        }
        $html .= '</select>';
        $html .= '</div>';
        $html .= '</div>';

        // Lấy khối quảng cáo
        $html1 = "<select class=\"form-control\" name=\"config_idplanbanner\">\n";
        $html1 .= "<option value=\"\">" . $lang_block['idplanbanner'] . "</option>\n";
        $query = "SELECT * FROM " . NV_BANNERS_GLOBALTABLE . "_plans WHERE (blang='" . NV_LANG_DATA . "' OR blang='') ORDER BY title ASC";
        $result = $db->query($query);

        while ($row_bpn = $result->fetch()) {
            $value = $row_bpn['title'] . " (";
            $value .= ((!empty($row_bpn['blang']) and isset($language_array[$row_bpn['blang']])) ? $language_array[$row_bpn['blang']]['name'] : $lang_block['blang_all']) . ", ";
            $value .= $row_bpn['form'] . ", ";
            $value .= $row_bpn['width'] . "x" . $row_bpn['height'] . "px";
            $value .= ")";
            $sel = ($data_block['idplanbanner'] == $row_bpn['id']) ? ' selected' : '';

            $html1 .= "<option value=\"" . $row_bpn['id'] . "\" " . $sel . ">" . $value . "</option>\n";
        }

        $html1 .= "</select>\n";
        $html .= '<div class="form-group"><label class="control-label col-sm-6">' . $lang_block['idplanbanner'] . ':</label><div class="col-sm-18">' . $html1 . '</div></div>';

        return $html;
    }

    /**
     * nv_block_config_home_shops_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_home_shops_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['idplanbanner'] = $nv_Request->get_int('config_idplanbanner', 'post', 0);
        $return['config']['numrow'] = $nv_Request->get_int('config_numrow', 'post', 10);
        $return['config']['catid'] = $nv_Request->get_int('config_catid', 'post', 0);
        $return['config']['menuid'] = $nv_Request->get_int('config_menuid', 'post', 0);
        return $return;
    }

    /**
     * nv_block_home_shops()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_home_shops($block_config)
    {
        global $site_mods, $db_config;

        $block_config['module'] = 'shops';
        if (empty($block_config['numrow'])) {
            $block_config['numrow'] = 10;
        }
        $module = $block_config['module'];

        if (isset($site_mods[$module])) {
            global $db, $global_config, $nv_Cache, $module_name, $global_array_shops_cat, $array_cat_shops, $db_config, $module_config, $global_array_group;

            $module_data = $site_mods[$module]['module_data'];
            $module_file = $site_mods[$module]['module_file'];
            $module_upload = $site_mods[$module]['module_upload'];
            $pro_config = $module_config[$module];

            $array_cat_shops = [];
            if ($module != $module_name) {
                $sql = "SELECT catid, parentid, lev, " . NV_LANG_DATA . "_title AS title, " . NV_LANG_DATA . "_alias AS alias, viewcat, numsubcat, subcatid, numlinks, " . NV_LANG_DATA . "_description AS description, inhome, " . NV_LANG_DATA . "_keywords AS keywords, groups_view FROM " . $db_config['prefix'] . "_" . $module_data . "_catalogs ORDER BY sort ASC";

                $list = $nv_Cache->db($sql, "catid", $module);
                foreach ($list as $row) {
                    $array_cat_shops[$row['catid']] = array(
                        "catid" => $row['catid'],
                        "parentid" => $row['parentid'],
                        "title" => $row['title'],
                        "alias" => $row['alias'],
                        "link" => NV_BASE_SITEURL . "index.php?" . NV_LANG_VARIABLE . "=" . NV_LANG_DATA . "&amp;" . NV_NAME_VARIABLE . "=" . $module . "&amp;" . NV_OP_VARIABLE . "=" . $row['alias'],
                        "viewcat" => $row['viewcat'],
                        "numsubcat" => $row['numsubcat'],
                        "subcatid" => $row['subcatid'],
                        "numlinks" => $row['numlinks'],
                        "description" => $row['description'],
                        "inhome" => $row['inhome'],
                        "keywords" => $row['keywords'],
                        "groups_view" => $row['groups_view'],
                        'lev' => $row['lev']
                    );
                }
                unset($list, $row);
                $global_array_shops_cat = $array_cat_shops;
            } else {
                $array_cat_shops = $global_array_shops_cat;
            }

            if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.home_shops.tpl')) {
                $block_theme = $global_config['module_theme'];
            } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.home_shops.tpl')) {
                $block_theme = $global_config['site_theme'];
            } else {
                $block_theme = 'default';
            }

            require NV_ROOTDIR . '/themes/' . $block_theme . '/language/' . NV_LANG_INTERFACE . '.php';

            $xtpl = new XTemplate('global.home_shops.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
            $xtpl->assign('TEMPLATE', $block_theme);
            $xtpl->assign('CONFIG', $block_config);
            $xtpl->assign('LINK_MODULE', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module);
            $xtpl->assign('LANG', $lang_module);
            $xtpl->assign('NV_BASE_SITEURL', NV_BASE_SITEURL);
            $xtpl->assign('NV_ASSETS_DIR', NV_ASSETS_DIR);

            if (!isset($array_cat_shops[$block_config['catid']])) {
                return '';
            }

            $cat = $array_cat_shops[$block_config['catid']];

            $xtpl->assign('LINK_CAT', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $cat['alias']);

            // Các menu
            $list_menus = array();
            if (!empty($block_config['menuid'])) {
                $sql = 'SELECT id, parentid, title, link, icon, note, subitem, groups_view, module_name, op, target, css, active_type
                FROM ' . NV_PREFIXLANG . '_menu_rows
                WHERE status=1 AND mid=' . $block_config['menuid'] . ' ORDER BY weight ASC';
                $list = $nv_Cache->db($sql, '', 'menu');

                foreach ($list as $rowmenu) {
                    if (nv_user_in_groups($rowmenu['groups_view'])) {
                        if ($rowmenu['link'] != '' and $rowmenu['link'] != '#') {
                            $rowmenu['link'] = nv_url_rewrite(nv_unhtmlspecialchars($rowmenu['link']), true);
                            switch ($rowmenu['target']) {
                                case 1:
                                    $rowmenu['target'] = '';
                                    break;
                                case 3:
                                    $rowmenu['target'] = ' onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,\');return false;"';
                                    break;
                                default:
                                    $rowmenu['target'] = ' onclick="this.target=\'_blank\'"';
                            }
                        } else {
                            $rowmenu['target'] = '';
                        }

                        if (!empty($rowmenu['icon']) and file_exists(NV_UPLOADS_REAL_DIR . '/menu/' . $rowmenu['icon'])) {
                            $rowmenu['icon'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/menu/' . $rowmenu['icon'];
                        } else {
                            $rowmenu['icon'] = '';
                        }
                        if (!isset($rowmenu['parentid'])) {
                            $list_menus[$rowmenu['parentid']] = [];
                        }
                        $list_menus[$rowmenu['parentid']][$rowmenu['id']] = array(
                            'id' => $rowmenu['id'],
                            'parentid' => $rowmenu['parentid'],
                            'subcats' => $rowmenu['subitem'],
                            'title' => $rowmenu['title'],
                            'target' => $rowmenu['target'],
                            'note' => empty($rowmenu['note']) ? $rowmenu['title'] : $rowmenu['note'],
                            'link' => $rowmenu['link'],
                            'icon' => $rowmenu['icon'],
                            'html_class' => $rowmenu['css']
                        );
                    }
                }
            }
            if (!empty($list_menus[0])) {
                foreach ($list_menus[0] as $menu) {
                    $xtpl->assign('MENU', $menu);
                    $xtpl->parse('main.menu.loop');
                }
                $xtpl->parse('main.menu');
            }

            // Xuất thông tin cat
            $xtpl->assign('CAT', $cat);

            // Cache các items để giảm truy vấn CSDL vì hàm nv_get_price
            $html_items = '';
            if (!defined('NV_IS_MODADMIN')) {
                $cache_file = NV_LANG_DATA . '_' . $block_theme . '_shopscat_' . $cat['catid'] . '_' . NV_CACHE_PREFIX . '.cache';
                if (($cache = $nv_Cache->getItem($module, $cache_file)) != false) {
                    $html_items = $cache;
                }
            }

            if (empty($html_items)) {
                // Lấy hết danh mục con
                global $array_cat;
                $array_cat = [];
                $array_cat = GetCatidInParent($cat['catid'], true);

                $db->sqlreset()->from($db_config['prefix'] . '_' . $module_data . '_rows t1')
                ->where('listcatid IN (' . implode(',', $array_cat) . ') AND inhome=1 AND status =1');
                $db->select('id, listcatid, publtime, ' . NV_LANG_DATA . '_title, ' . NV_LANG_DATA . '_alias, ' . NV_LANG_DATA . '_hometext,
                homeimgalt, homeimgfile, homeimgthumb, product_code, product_number, product_price, money_unit,
                discount_id, showprice, ' . NV_LANG_DATA . '_gift_content, gift_from, gift_to')
                ->order('id DESC')
                ->limit($block_config['numrow']);

                $result = $db->query($db->sql());
                $data_pro = array();

                while (list ($id, $listcatid, $publtime, $title, $alias, $hometext, $homeimgalt, $homeimgfile,
                    $homeimgthumb, $product_code, $product_number, $product_price, $money_unit, $discount_id,
                    $showprice, $gift_content, $gift_from, $gift_to) = $result->fetch(3)) {
                    if ($homeimgthumb == 1) {
                        $thumb = NV_BASE_SITEURL . NV_FILES_DIR . '/' . $module_upload . '/' . $homeimgfile;
                    } elseif ($homeimgthumb == 2) {
                        $thumb = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $module_upload . '/' . $homeimgfile;
                    } elseif ($homeimgthumb == 3) {
                        $thumb = $homeimgfile;
                    } else {
                        $thumb = NV_BASE_SITEURL . 'themes/default/images/' . $module_file . '/no-image.jpg';
                    }

                    $data_pro[] = array(
                        'id' => $id,
                        'listcatid' => $listcatid,
                        'publtime' => $publtime,
                        'title' => $title,
                        'alias' => $alias,
                        'hometext' => $hometext,
                        'homeimgalt' => $homeimgalt,
                        'homeimgthumb' => $thumb,
                        'product_code' => $product_code,
                        'product_number' => $product_number,
                        'product_price' => $product_price,
                        'discount_id' => $discount_id,
                        'money_unit' => $money_unit,
                        'showprice' => $showprice,
                        'gift_content' => $gift_content,
                        'gift_from' => $gift_from,
                        'gift_to' => $gift_to,
                        'newday' => $cat['newday'],
                        'link_pro' => NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $cat['alias'] . '/' . $alias . $global_config['rewrite_exturl'],
                        'link_order' => NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=setcart&amp;id=' . $id
                    );
                }

                foreach ($data_pro as $row) {
                    $xtpl->assign('ROW', $row);

                    // Xử lý hiển thị giá
                    $price = nv_get_price($row['id'], $pro_config['money_unit']);
                    if (!empty($pro_config['active_price'])) {
                        if (!empty($row['showprice']) and !empty($row['product_price'])) {
                            $xtpl->assign('PRICE', $price);
                            if ($row['discount_id'] and $price['discount_percent'] > 0) {
                                // Hiển thị giá gốc
                                $xtpl->parse('items.loop.price_real');

                                // Hiển thị % giảm giá
                                if ($price['discount_unit'] == '%') {
                                    $xtpl->parse('items.loop.discount');
                                }
                            }
                            $xtpl->parse('items.loop.price');
                        } else {
                            $xtpl->parse('items.loop.contact');
                        }
                    }

                    // Hiển thị còn hàng, hết hàng
                    if ($row['product_number'] > 0) {
                        $xtpl->parse('items.loop.conhang');

                        // Kiem tra nhom bat buoc chon khi dat hang
                        $listgroupid = GetGroupID($row['id']);
                        $group_requie = 0;
                        if (!empty($listgroupid) and !empty($global_array_group)) {
                            foreach ($global_array_group as $groupinfo) {
                                if ($groupinfo['in_order']) {
                                    $group_requie = 1;
                                    break;
                                }
                            }
                        }
                        $group_requie = $pro_config['active_order_popup'] ? 1 : $group_requie;
                        $xtpl->assign('GROUP_REQUIE', $group_requie);

                        $xtpl->parse('items.loop.order');
                    } else {
                        $xtpl->parse('items.loop.hethang');
                    }

                    // Quà tặng
                    if ($pro_config['active_gift'] and !empty($row['gift_content']) and (NV_CURRENTTIME >= $row['gift_from'] or empty($row['gift_from'])) and (NV_CURRENTTIME <= $row['gift_to'] or empty($row['gift_to']))) {
                        $xtpl->assign('GIFT_CONTENT', nv_nl2br($row['gift_content']));
                        $xtpl->parse('items.loop.gift');
                    }

                    $xtpl->parse('items.loop');
                }

                $xtpl->parse('items');
                $html_items = $xtpl->text('items');

                if (!defined('NV_IS_MODADMIN') and !empty($html_items)) {
                    $nv_Cache->setItem($module, $cache_file, $html_items);
                }
            }

            $xtpl->assign('HTML_ITEMS', $html_items);

            // Lấy và hiển thị quảng cáo
            if ($global_config['idsite']) {
                $xmlfile = NV_ROOTDIR . '/' . NV_DATADIR . '/site_' . $global_config['idsite'] . '_bpl_' . $block_config['idplanbanner'] . '.xml';
            } else {
                $xmlfile = NV_ROOTDIR . '/' . NV_DATADIR . '/bpl_' . $block_config['idplanbanner'] . '.xml';
            }

            if (file_exists($xmlfile)) {
                $xml = simplexml_load_file($xmlfile);
                if ($xml !== false) {
                    $width_banners = intval($xml->width);
                    $height_banners = intval($xml->height);
                    $array_banners = $xml->banners->banners_item;

                    $array_banners_content = array();

                    foreach ($array_banners as $banners) {
                        $banners = (array) $banners;
                        if ($banners['publ_time'] < NV_CURRENTTIME and ($banners['exp_time'] == 0 or $banners['exp_time'] > NV_CURRENTTIME)) {
                            $banners['file_height'] = round($banners['file_height'] * $width_banners / $banners['file_width']);
                            $banners['file_width'] = $width_banners;
                            if (!empty($banners['imageforswf']) and !empty($client_info['is_mobile'])) {
                                $banners['file_name'] = $banners['imageforswf'];
                                $banners['file_ext'] = nv_getextension($banners['file_name']);
                            }
                            $banners['file_alt'] = (!empty($banners['file_alt'])) ? $banners['file_alt'] : $banners['title'];
                            $banners['file_image'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . NV_BANNER_DIR . '/' . $banners['file_name'];
                            $banners['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=banners&amp;' . NV_OP_VARIABLE . '=click&amp;id=' . $banners['id'] . '&amp;s=' . md5($banners['id'] . NV_CHECK_SESSION);
                            $array_banners_content[] = $banners;
                        }
                    }

                    if (!empty($array_banners_content)) {
                        if ($xml->form == 'random') {
                            shuffle($array_banners_content);
                        }
                        unset($xml, $array_banners);

                        foreach ($array_banners_content as $banners) {
                            $xtpl->assign('BANNER', $banners);

                            if ($banners['file_ext'] == 'swf') {
                                // NOTHING
                            } elseif (!empty($banners['file_click'])) {
                                $xtpl->parse('main.banner.type_image_link');
                            } else {
                                $xtpl->parse('main.banner.type_image');
                            }

                            $xtpl->parse('main.banner');
                        }
                    }
                }
            }

            $xtpl->parse('main');
            return $xtpl->text('main');
        }
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_home_shops($block_config);
}
