<!-- BEGIN: main -->
<div class="home-news-student">
    <!-- BEGIN: first -->
    <div class="item-first">
        <a class="thumb mb-3" href="{ROW.link}"{ROW.target_blank} style="background-image: url('{ROW.imgsourcebig}');">
            <img src="{ROW.imgsourcebig}" alt="{ROW.title}" class="hidden">
        </a>
        <h3 class="mb-2"><a href="{ROW.link}"{ROW.target_blank}>{ROW.title}</a></h3>
    </div>
    <!-- END: first -->
    <div class="item-others">
        <!-- BEGIN: loop -->
        <div class="item mb-3 clearfix">
            <h3><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <a href="{ROW.link}"{ROW.target_blank}>{ROW.title}</a></h3>
        </div>
        <!-- END: loop -->
    </div>
</div>
<!-- END: main -->
