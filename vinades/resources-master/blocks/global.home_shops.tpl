<!-- BEGIN: main -->
<div class="catshops-rows" id="catshops-rows-{CAT.catid}">
    <div class="catshops-rows-header">
        <h2><a href="{CAT.link}">{CAT.title}</a></h2>
        <div class="catsubs">
            <!-- BEGIN: menu -->
            <ul>
                <!-- BEGIN: loop -->
                <li><a href="{MENU.link}">{MENU.title}</a></li>
                <!-- END: loop -->
            </ul>
            <!-- END: menu -->
        </div>
        <div class="catmore">
            <a href="{LINK_CAT}">{LANG.viewall}</a>
        </div>
    </div>
    <div class="catshops-rows-body">
        <div class="row">
            <div class="col-md-19">
                {HTML_ITEMS}
            </div>
            <div class="col-md-5">
                <!-- BEGIN: banner -->
                <div class="row-banner">
                    <!-- BEGIN: type_image  -->
                    <img class="lazy" src="{NV_BASE_SITEURL}{NV_ASSETS_DIR}/images/pix.gif" data-src="{BANNER.file_image}" alt="{BANNER.title}">
                    <!-- END: type_image  -->
                    <!-- BEGIN: type_image_link  -->
                    <a href="{BANNER.link}" target="{BANNER.target}"><img class="lazy" src="{NV_BASE_SITEURL}{NV_ASSETS_DIR}/images/pix.gif" data-src="{BANNER.file_image}" alt="{BANNER.title}"></a>
                    <!-- END: type_image_link  -->
                </div>
                <!-- END: banner -->
            </div>
        </div>
    </div>
</div>
<!-- END: main -->

Khúc bên dưới này để xuất và cache các sản phẩm

<!-- BEGIN: items -->
<div class="row-items" id="catshops-rows-{CAT.catid}-rows">
    <!-- BEGIN: loop -->
    <div class="item" id="catshops-rows-item-{ROW.id}">
        <div>
            <div class="item-code">{LANG.procode} {ROW.product_code}</div>
            <div class="item-img" data-catid="{CAT.catid}" data-id="{ROW.id}">
                <a href="{ROW.link_pro}"><img class="lazy" src="{NV_BASE_SITEURL}{NV_ASSETS_DIR}/images/pix.gif" data-src="{ROW.homeimgthumb}" alt="{ROW.homeimgalt}"></a>
            </div>
            <div class="item-price">
                <!-- BEGIN: price -->
                {PRICE.sale_format}
                <!-- END: price -->
            </div>
            <div class="item-price-old">
                <!-- BEGIN: contact -->
                {LANG.price_contact}
                <!-- END: contact -->
                <!-- BEGIN: price_real -->
                <span class="realpr">{PRICE.price_format} {ROW.money_unit}</span>
                <!-- END: price_real -->
                <!-- BEGIN: discount -->
                <span class="discount">-{PRICE.discount_percent}{PRICE.discount_unit}</span>
                <!-- END: discount -->
            </div>
            <h3 class="item-title">
                <a href="{ROW.link_pro}">{ROW.title}</a>
            </h3>
            <div class="item-tools">
                <div class="st">
                    <!-- BEGIN: conhang --><span class="proch"><i class="fa fa-check" aria-hidden="true"></i> {LANG.pro_conhang}</span><!-- END: conhang -->
                    <!-- BEGIN: hethang --><i class="prohh" aria-hidden="true"></i> <span class="text-danger">{LANG.pro_hethang}</span><!-- END: hethang -->
                </div>
                <div class="ca">
                    <!-- BEGIN: order --><a href="javascript:void(0);" id="{ROW.id}" title="{ROW.title}" onclick="cartorder(this, {GROUP_REQUIE}, '{ROW.link_pro}'); return !1;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> {LANG.cart}</a><!-- END: order -->
                </div>
            </div>
        </div>
        <div class="item-protip" id="catshops-rows-itemtip-{ROW.id}">
            <div>
                <div class="tname">{ROW.title}</div>
                <div class="tline"></div>
                <div class="tcontent-row rflex center-rflex">
                    <div class="frtit">{LANG.pro_tipprice}:</div>
                    <div class="frct">
                        <span class="pro_tipprice">{PRICE.sale_format}</span>
                        <div>{LANG.isvat}</div>
                    </div>
                </div>
                <div class="tline"></div>
                <div class="tcontent-row">
                    <div class="rtit">{LANG.pro_hometext}:</div>
                    <div class="rct">{ROW.hometext}</div>
                </div>
                <!-- BEGIN: gift -->
                <div class="tline"></div>
                <div class="tcontent-row">
                    <div class="rtit">{LANG.pro_gift}:</div>
                    <div class="rct">{GIFT_CONTENT}</div>
                </div>
                <!-- END: gift -->
            </div>
        </div>
    </div>
    <!-- END: loop -->
</div>
<!-- END: items -->
