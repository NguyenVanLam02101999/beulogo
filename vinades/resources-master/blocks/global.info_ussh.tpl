<!-- BEGIN: main  -->
<div class="block-info-ussh">
    <p class="info-ussh-disc">{BLOCK_CONFIG.disc}</p>
    <div class="content">
        <div class="row row-info-ussh">
            <!-- BEGIN: column  -->
            <div class="fix_column">
                <p class="info-ussh-name">{title}</p>
                <p class="info-ussh-content">{value}</p>
            </div>
            <!-- END: column  -->
        </div>
    </div>
</div>
<script>
$(".fix_column").addClass("col-xs-12 col-sm-{COL} col-md-{COL}");
</script>
<!-- END: main -->
