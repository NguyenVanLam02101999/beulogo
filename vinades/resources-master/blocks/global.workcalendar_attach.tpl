<!-- BEGIN: main -->
<div class="home-workcalendar-attach">
    <!-- BEGIN: loop -->
    <div class="item">
        <a class="text-gray" href="{ROW.link}">{ARRAY.title} {LANG.block_week} {ROW.week}-{ROW.year} {ROW.weekdisplay}</a>
    </div>
    <!-- END: loop -->
</div>
<!-- END: main -->
