<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate 3/9/2010 23:25
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (!nv_function_exists('nv_block_workcalendar_attach')) {

    /**
     * nv_block_config_workcalendar_attach()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_workcalendar_attach($module, $data_block, $lang_block)
    {
        global $nv_Cache, $site_mods, $nv_Request;

        // Xuất nội dung khi có chọn module
        if ($nv_Request->isset_request('loadajaxdata', 'get')) {
            $module = $nv_Request->get_title('loadajaxdata', 'get', '');

            $html = '<div class="form-group">';
            $html .= '	<label class="control-label col-sm-6">' . $lang_block['numrow'] . ':</label>';
            $html .= '	<div class="col-sm-9"><input type="text" name="config_numrow" class="form-control" value="' . $data_block['numrow'] . '"/></div>';
            $html .= '</div>';

            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['catid'] . ':</label>';

            $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_categories WHERE status=1 ORDER BY weight ASC';
            $list = $nv_Cache->db($sql, '', $module);

            $html .= '<div class="col-sm-18">';
            $html .= '<select class="form-control" name="config_catid">';
            foreach ($list as $l) {
                $html .= '<option value="' . $l['id'] . '"' . ($l['id'] == $data_block['catid'] ? ' selected="selected"' : '') . '>' . $l['title'] . '</option>';
            }
            $html .= '</select>';
            $html .= '</div>';
            $html .= '</div>';

            nv_htmlOutput($html);
        }

        $html = '';
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['selectmod'] . ':</label>';
        $html .= '<div class="col-sm-9">';
        $html .= '<select name="config_selectmod" class="form-control">';
        $html .= '<option value="">--</option>';

        foreach ($site_mods as $title => $mod) {
            if ($mod['module_file'] == 'workcalendar') {
                $html .= '<option value="' . $title . '"' . ($title == $data_block['selectmod'] ? ' selected="selected"' : '') . '>' . $mod['custom_title'] . '</option>';
            }
        }

        $html .= '</select>';

        $html .= '
        <script type="text/javascript">
        $(\'[name="config_selectmod"]\').change(function() {
            var mod = $(this).val();
            var file_name = $("select[name=file_name]").val();
            var module_type = $("select[name=module_type]").val();
            var blok_file_name = "";
            if (file_name != "") {
                var arr_file = file_name.split("|");
                if (parseInt(arr_file[1]) == 1) {
                    blok_file_name = arr_file[0];
                }
            }
            if (mod != "") {
                $.get(script_name + "?" + nv_name_variable + "=" + nv_module_name + \'&\' + nv_lang_variable + "=" + nv_lang_data + "&" + nv_fc_variable + "=block_config&bid=" + bid + "&module=" + module_type + "&selectthemes=" + selectthemes + "&file_name=" + blok_file_name + "&loadajaxdata=" + mod + "&nocache=" + new Date().getTime(), function(theResponse) {
                    $("#block_config").append(theResponse);
                });
            }
        });
        $(function() {
            $(\'[name="config_selectmod"]\').change();
        });
        </script>
        ';

        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * nv_block_config_workcalendar_attach_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_workcalendar_attach_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['selectmod'] = $nv_Request->get_title('config_selectmod', 'post', '');
        $return['config']['numrow'] = $nv_Request->get_int('config_numrow', 'post', 0);
        $return['config']['catid'] = $nv_Request->get_int('config_catid', 'post', 0);
        return $return;
    }

    /**
     * nv_block_workcalendar_attach()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_workcalendar_attach($block_config)
    {
        global $site_mods;

        $block_config['module'] = $block_config['selectmod'];
        $module = $block_config['module'];

        if (isset($site_mods[$module]) and !empty($block_config['catid'])) {
            global $db, $global_config, $nv_Cache, $module_config;

            $module_array_cat = array();
            $module_data = $site_mods[$module]['module_data'];
            $module_upload = $site_mods[$module]['module_upload'];
            $module_file = $site_mods[$module]['module_file'];

            // Lấy ra cái bộ lịch
            $sql = "SELECT * FROM " . NV_PREFIXLANG . "_" . $module_data . "_categories WHERE status=1 AND id=" . $block_config['catid'];
            $list = $nv_Cache->db($sql, '', $module);

            if (empty($list)) {
                return '';
            }
            $array = $list[0];
            unset($list);

            // Lấy ngôn ngữ
            $lang_module = [];
            include NV_ROOTDIR . '/modules/' . $module_file . '/language/' . NV_LANG_INTERFACE . '.php';

            // Lấy đính kèm tuần
            $sql = "SELECT * FROM " . NV_PREFIXLANG . "_" . $module_data . "_week_attach
            WHERE catid=" . $block_config['catid'] . " ORDER BY year DESC, week DESC LIMIT 0," . $block_config['numrow'];
            $list = $nv_Cache->db($sql, '', $module);

            if (!empty($list)) {
                if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.workcalendar_attach.tpl')) {
                    $block_theme = $global_config['module_theme'];
                } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.workcalendar_attach.tpl')) {
                    $block_theme = $global_config['site_theme'];
                } else {
                    $block_theme = 'default';
                }
   
                include NV_ROOTDIR . '/themes/' . $block_theme . '/language/' . NV_LANG_INTERFACE . '.php';

                $xtpl = new XTemplate('global.workcalendar_attach.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
                $xtpl->assign('NV_BASE_SITEURL', NV_BASE_SITEURL);
                $xtpl->assign('TEMPLATE', $block_theme);
                $xtpl->assign('CONFIG', $block_config);
                $xtpl->assign('LANG', $lang_module);
                $xtpl->assign('ARRAY', $array);
                $xtpl->assign('LINK_MODULE', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module);

                $time_per_week = 86400 * 7;
                foreach ($list as $row) {
                    $row['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $array['alias'] . '/' . change_alias($lang_module['op_viewweek']) . '/' . change_alias($lang_module['week_reviews_week']) . '-' . $row['week'] . '-' . $row['year'];

                    $time_start_year = mktime(0, 0, 0, 1, 1, $row['year']);
                    $time_first_week = $time_start_year - (86400 * (date('N', $time_start_year) - 1));
                    $textfrom = nv_date('d/m/Y', $time_first_week + ($row['week'] - 1) * $time_per_week);
                    $textto = nv_date('d/m/Y', $time_first_week + ($row['week'] - 1) * $time_per_week + $time_per_week - 1);
                    $textweek = strtolower($lang_module['from']) . ' ' . $textfrom . ' ' . $lang_module['to'] . ' ' . $textto;
                    $row['weekdisplay'] = trim($textweek);
                    $xtpl->assign('ROW', $row);
                    $xtpl->parse('main.loop');
                }

                $xtpl->parse('main');
                return $xtpl->text('main');
            }
        }
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_workcalendar_attach($block_config);
}
