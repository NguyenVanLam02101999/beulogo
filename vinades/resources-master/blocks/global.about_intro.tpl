<!-- BEGIN: main -->
<div class="widget-about-intro">
    <div class="row">
        <div class="col-sm-14 col-md-14">
            <h2><span>{DATA.intro_title}</span></h2>
            <div class="i-body">
                {DATA.intro_text}
            </div>
        </div>
        <div class="col-sm-10 col-md-10">
            <!-- BEGIN: video -->
            <video controls="" height="260" width="100%">
                <source src="{DATA.intro_video}" type="video/mp4" />
                <source src="{DATA.intro_video}" type="video/ogg" />
            </video>
            <!-- END: video -->
        </div>
    </div>
</div>
<!-- END: main -->
