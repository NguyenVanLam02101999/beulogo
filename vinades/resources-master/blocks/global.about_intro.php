<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC <contact@vinades.vn>
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Sun, 04 May 2014 12:41:32 GMT
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (!nv_function_exists('nv_menu_theme_about_intro')) {
    /**
     * nv_menu_theme_about_intro_config()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_menu_theme_about_intro_config($module, $data_block, $lang_block)
    {
        $html = '<div class="form-group">';
        $html .= '	<label class="control-label col-sm-6">' . $lang_block['intro_title'] . ':</label>';
        $html .= '	<div class="col-sm-18"><input type="text" name="config_intro_title" class="form-control" value="' . $data_block['intro_title'] . '"/></div>';
        $html .= '</div>';

        if (defined('NV_EDITOR')) {
            require_once NV_ROOTDIR . '/' . NV_EDITORSDIR . '/' . NV_EDITOR . '/nv.php';
        }
        $data_block['intro_text'] = htmlspecialchars(nv_editor_br2nl($data_block['intro_text']));
        if (defined('NV_EDITOR') and nv_function_exists('nv_aleditor')) {
            $data_block['intro_text'] = nv_aleditor('config_intro_text', '100%', '300px', $data_block['intro_text']);
        } else {
            $data_block['intro_text'] = '<textarea style="width:100%;height:300px" name="config_intro_text" class="form-control">' . $data_block['intro_text'] . '</textarea>';
        }

        $html .= '<div class="form-group">';
        $html .= '	<label class="control-label col-sm-6">' . $lang_block['intro_text'] . ':</label>';
        $html .= '	<div class="col-sm-18">' . $data_block['intro_text'] . '</div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '	<label class="control-label col-sm-6">' . $lang_block['intro_video'] . ':</label>';
        $html .= '	<div class="col-sm-18">
                        <div class="input-group">
                            <input type="text" id="iptAboutIntroVideo" name="config_intro_video" class="form-control" value="' . $data_block['intro_video'] . '"/>
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="pickAboutIntroVideo"><i class="fa fa-file-video-o" aria-hidden="true"></i></button>
                            </span>
                        </div>
                    </div>';
        $html .= '</div>';
        $html .= '
        <script>
        $("#pickAboutIntroVideo").click(function() {
            nv_open_browse(script_name + "?" + nv_name_variable + "=upload&popup=1&area=iptAboutIntroVideo&alt=&path=' . NV_UPLOADS_DIR . '&type=file", "NVImg", 850, 420, "resizable=no,scrollbars=no,toolbar=no,location=no,status=no");
        });
        </script>
        ';
        return $html;
    }

    /**
     * nv_menu_theme_about_intro_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_menu_theme_about_intro_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();

        $htmlcontent = $nv_Request->get_editor('config_intro_text', '', NV_ALLOWED_HTML_TAGS);
        $htmlcontent = strtr($htmlcontent, [
            "\r\n" => '',
            "\r" => '',
            "\n" => ''
        ]);

        $return['config']['intro_title'] = $nv_Request->get_title('config_intro_title', 'post', '');
        $return['config']['intro_video'] = $nv_Request->get_title('config_intro_video', 'post', '');
        $return['config']['intro_text'] = $htmlcontent;
        return $return;
    }

    /**
     * nv_menu_theme_about_intro()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_menu_theme_about_intro($block_config)
    {
        global $global_config, $site_mods, $lang_global;

        if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.about_intro.tpl')) {
            $block_theme = $global_config['module_theme'];
        } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.about_intro.tpl')) {
            $block_theme = $global_config['site_theme'];
        } else {
            $block_theme = 'default';
        }

        $xtpl = new XTemplate('global.about_intro.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
        $xtpl->assign('NV_BASE_SITEURL', NV_BASE_SITEURL);
        $xtpl->assign('LANG', $lang_global);
        $xtpl->assign('BLOCK_THEME', $block_theme);
        $xtpl->assign('DATA', $block_config);

        if (!empty($block_config['intro_video'])) {
            $xtpl->parse('main.video');
        }

        $xtpl->parse('main');
        return $xtpl->text('main');
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_menu_theme_about_intro($block_config);
}
