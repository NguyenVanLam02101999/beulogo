<!-- BEGIN: main -->
<div class="im-flex-section-row">
    <div class="wraper">
        <div class="container">
            <div class="im-flex-main-news">
                <!-- BEGIN: loop -->
                <div class="item">
                    <div class="item-content">
                        <a class="img" style="background-image: url('{ROW.imgfull}');"><img src="{ROW.imgfull}" alt="{ROW.title}"></a>
                        <div class="item-inner">
                            <h3><a href="{ROW.link}" title="{ROW.title}">{ROW.title_trim}</a></h3>
                            <div class="htext">{ROW.hometext_clean}</div>
                        </div>
                    </div>
                </div>
                <!-- END: loop -->
            </div>
            <div class="im-flex-main-news-btn text-center">
                <a href="{LINK_MORE}" class="btn btn-lg">{LANG.view_more_cource}</a>
            </div>
        </div>
    </div>
</div>
<!-- END: main -->
