<!-- BEGIN: main -->
<div class="our_proven_step">
     <div class="col-md-24">
        <h3 class="title_top">{CONFIG.title_top}</h3>
    </div>
    <!-- BEGIN: loop -->
        <div class="col-md-8">
            <div class="hinhanh text-center">
                <a href="{VIEW.link}" title="{VIEW.title}" class="hinhanh_how_work_it"><img alt="{TITLE}" src="{VIEW.image}" /></a>
            </div>
            <div class="tieude">
                <h3>
                    {VIEW.title}
                </h3>
            </div>
            <div class="mota">
                {VIEW.bodytext}
            </div>

        </div>
    <!-- END: loop -->
</div>
<!-- END: main -->
