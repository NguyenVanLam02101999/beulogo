<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2015 VINADES ., JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Jan 10, 2011 6:04:30 PM
 */

if (!defined('NV_MAINFILE'))
    die('Stop!!!');

if (!nv_function_exists('nv_block_global_out_proven')) {
    /**
     * nv_block_config_text_banner()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_out_proven($module, $data_block, $lang_block)
    {
        global $db_config, $nv_Cache, $site_mods;
        
        $html = $block = '';
        $link  = NV_BASE_SITEURL . 'assets/js/select2/';
        $js1 = $link.'select2.min.js';
        $js2 = $link.'i18n/vi.js';
        $css = $link.'select2.min.css';
        $html .= '<script type="text/javascript" src="'.$js1.'"></script>';
        $html .= '<script type="text/javascript" src="'.$js2.'"></script>';
        $html .= '<link rel="stylesheet" type="text/css" href="'.$css.'">';
        $html .= '<script type="text/javascript"></script>';
        $html .= '<div class="form-group">';
        $html .= '  <label class="control-label col-sm-6">' . $lang_block['module'] . ':</label>';
        $html .= '  <div class="col-sm-18"><select name="config_module_name " id="config_module_name" class="form-control">';
        $sql = 'SELECT title, module_data, custom_title FROM ' . $db_config['prefix'] . '_' . NV_LANG_DATA . '_modules WHERE module_file = "page" and module_data = "about"';
        $list = $nv_Cache->db( $sql, 'title', $module );
        foreach( $list as $l )
        {
            $sel = ( $data_block['module_name'] == $l['title'] ) ? ' selected' : '';
            $html .= '<option value="' . $l['title'] . '" ' . $sel . '>' . $l['custom_title'] . '</option>';
            $block .= nv_get_blocklist_out_proven( $l['title'] );
        }
        $post = '["'.implode('","', $data_block['posts']).'"]';

        $html .= '  </select>';
        $html .= '  <div style="display: none">' . $block . '</div>';
        $html .= '  </div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">Tiêu đề:</label>';
        $html .= '<div class="col-sm-18"><input class="form-control" name="tieude" type="text" value="'.$data_block['title_top'].'"></div>';
        $html .= '</div>';
        
        $html .= '<script type="text/javascript">';
        $html .= '  $("#div-blockid").html( $("#select_" + $(\'#config_module_name\').val() ).html() ).find("select").attr("name", "config_posts[]");';
        $html .= '  $("#config_module_name").change(function() {';
        $html .= '      $("#div-blockid").html("<img src=\'' . NV_BASE_SITEURL . NV_ASSETS_DIR . '/images/load_bar.gif\' />").html( $("#select_" + $(this).val() ).html() ).find("select").attr("name", "config_posts[]");';
        $html .= '  }); $(".baiviet_select2").select2(); $(".baiviet_select2").select2().val('.$post.').trigger("change")';
        // 
        $html .= '</script>';
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['posts'] . ':</label>';
        $html .= '<div class="col-sm-18" id="div-blockid"></div>';
        $html .= '</div>';
        return $html;
    }

     function nv_get_blocklist_out_proven( $module )
    {
        global $site_mods, $data_block, $nv_Cache, $lang_block;

        $html = '';
        $html .= '<div id="select_' . $module . '">';
        $html .= '<select  class="form-control baiviet_select2 " multiple="multiple" >';
        $html .= '<option value="0">---' . $lang_block['posts_c'] . '---</option>';
        $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . ' ORDER BY weight ASC';
        $list = $nv_Cache->db($sql, '', $module);
        foreach ($list as $l) {
            $html .= '<option value="' . $l['id'] . '">' . $l['title'] . '</option>';
        }
        $html .= '</select>';
        $html .= '</div>';

        return $html;
    }

    /**
     * nv_block_config_text_banner_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_out_proven_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['module_name'] = $nv_Request->get_title('config_module_name', 'post', 'about');
        // $return['config']['posts'] = $nv_Request->get_int('config_posts', 'post', 0);
        $return['config']['posts'] = $_POST['config_posts'];
        $return['config']['bodytext_length'] = $nv_Request->get_int('config_bodytext_length', 'post', 20);
        $return['config']['title_top'] = $nv_Request->get_title('tieude', 'post', "OUR PROVEN STEPS");
        return $return;
    }

    /**
     * nv_block_global_text_banner()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_global_out_proven($block_config)
    {
        global $global_config, $site_mods, $module_config, $nv_Cache, $db, $lang_block, $module_name, $db_slave, $lang_block;
        $module = $block_config['module_name'];
        $post = implode(",", $block_config['posts']);
        if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.out_proven.tpl')) {
                $block_theme = $global_config['module_theme'];
        } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.out_proven.tpl')) {
            $block_theme = $global_config['site_theme'];
        } else {
            $block_theme = 'default';
        }
            
        $xtpl = new XTemplate('global.out_proven.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
        

        if(empty($post)){
            $arr = [];
        }else{
            $sql = 'SELECT id, title, alias, image, imagealt, description, bodytext, keywords, add_time, edit_time FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . ' WHERE id IN ('.$post.')';
            $arr = $db->query($sql)->fetchAll();
            foreach ($arr as $key => $value) {
                $link = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $value['alias'] . $global_config['rewrite_exturl'];
                if (! empty($value['image']) and file_exists(NV_ROOTDIR . NV_BASE_SITEURL . NV_ASSETS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $value['image'])) {
                    $images = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $value['image'];
                } elseif (! empty($value['image'])) {
                    $images = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $value['image'];
                }else{
                    $images = NV_BASE_SITEURL . '/themes/' . $global_config['site_theme'] . '/images/no_image_about.png';
                }
                $arr[$key]['image'] = $images;
                $arr[$key]['link'] = $link;
                
            }
            foreach ($arr as $key => $value) {
                $xtpl->assign('VIEW', $value);
                $xtpl->parse('main.loop');
            }
        }
        $xtpl->assign('CONFIG', $block_config);
        $xtpl->parse('main');
        return $xtpl->text('main');
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_global_out_proven($block_config);
}
