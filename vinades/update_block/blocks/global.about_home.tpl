<!-- BEGIN: main -->
<div class="wraper row content reorder-xs">
    <div class="content_header">
        <span class="border_logic_header"></span>
        <div class="col-xs-24 col-sm-12 col-md-12 rotate" style="padding-left:0;">
            <div class="txt"><h2 class="content_header_title"><a  href="{LINK}" class="hover_red" title="{TITLE}">{TITLE}</a></h2></div>
            <div class="content_about">
                <p class="bodytext">{BODYTEXT}</p>
            </div>
        </div>
        <div class="col-xs-24 col-sm-12 col-md-12 rotate">
            <a href="{LINK}" title="{TITLE}" class="hinhanh_header"><img alt="{TITLE}" src="{IMAGES}" /></a>
        </div>
    </div>
</div>
<!-- END: main -->
