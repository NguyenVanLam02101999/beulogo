<!-- BEGIN: main -->
<div class="panel-body">
	<div class="featured-products">
		<!-- BEGIN: loop -->
		<div class="row">
			<div class="col-xs-24 col-sm-5 col-md-8">
				<!-- BEGIN: image_only --><img title="{ROW.title}" src="{ROW.image}" class="img-thumbnail"><!-- END: image_only -->
				<!-- BEGIN: image_link -->
				<div class="hidden hide_button">
					<span class="content_a_link">
					<span class="content_icon_left"><i class="fa fa-plus" aria-hidden="true"></i></span>
						<a href="{ROW.link}" title="{ROW.title}"<!-- BEGIN: target --> target="{ROW.target}"<!-- END: target -->><span>GET STARTED</span> <span class="content_icon_right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </span>
					</span>
				</div> 
				<a href="{ROW.link}" title="{ROW.title}"<!-- BEGIN: target --> target="{ROW.target}"<!-- END: target -->><img src="{ROW.image}" title="{ROW.title}" class="img-thumbnail"></a><!-- END: image_link -->
			</div>
			<div class="col-xs-24 col-sm-19 col-md-16 content_">
				<div class="center-block_product">
					<p class="tieude">{ROW.title}</p>
					<p class="mota">
						{ROW.description}
					</p>
					<p class="p_link">

					</p>
					
				</div>
			</div>
		</div>
		<!-- END: loop -->
	</div>
</div>
<!-- END: main -->