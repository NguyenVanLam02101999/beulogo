<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2015 VINADES ., JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Jan 10, 2011 6:04:30 PM
 */

if (!defined('NV_MAINFILE'))
    die('Stop!!!');

if (!nv_function_exists('nv_block_global_square')) {
    /**
     * nv_block_config_text_banner()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_square($module, $data_block, $lang_block)
    {
        $html = '';
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">Số lần lặp icon the chiều ngang:</label>';
        $html .= '<div class="col-sm-18"><input type="number" name="icon" min="5" requied class="form-control" value="' . ($data_block['countNumber'] != "" ? $data_block['countNumber'] : 5) . '"/></div>';
        $html .= '</div>';

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">Số lần lặp icon the chiều dọc:</label>';
        $html .= '<div class="col-sm-18"><input type="number" name="repeat_row" min="3" requied class="form-control" value="' . ($data_block['countNumberRow'] != "" ? $data_block['countNumberRow'] : 3) . '"/></div>';
        $html .= '</div>';
        
        return $html;
    }

    /**
     * nv_block_config_text_banner_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_square_submit($module, $lang_block)
    {
        global $nv_Request;
        $return                         = array();
        $return['error']                = array();
        $return['config']               = array();
        $return['config']['icon']       = $nv_Request->get_title('icon', 'post', '');
        $return['config']['repeat_row'] = $nv_Request->get_title('repeat_row', 'post', '');
        return $return;
    }

    /**
     * nv_block_global_text_banner()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_global_square($block_config)
    {
        global $global_config;
       

        if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.square.tpl')) {
            $block_theme = $global_config['module_theme'];
        } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.square.tpl')) {
            $block_theme = $global_config['site_theme'];
        } else {
            $block_theme = 'default';
        }

        $xtpl = new XTemplate('global.square.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
        $xtpl->assign('TEMPLATE', $block_theme);
        $xtpl->assign('CONFIG', $block_config);

        if($block_config['icon'] > 0){
            for($i = 0; $i < $block_config['icon']; $i++){
                $xtpl->assign('ICON', '<i class="fa fa-circle" aria-hidden="true"></i>');
                $xtpl->parse('main.loop');
            }
        }

        $xtpl->parse('main');
        return $xtpl->text('main');
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_global_square($block_config);
}
