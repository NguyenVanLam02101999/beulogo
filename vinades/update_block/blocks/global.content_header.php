<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2015 VINADES ., JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Jan 10, 2011 6:04:30 PM
 */

if (!defined('NV_MAINFILE'))
    die('Stop!!!');

if (!nv_function_exists('nv_block_global_content_header')) {
    /**
     * nv_block_config_text_banner()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_content_header($module, $data_block, $lang_block)
    {

        if (defined('NV_EDITOR')) {
            require NV_ROOTDIR . '/' . NV_EDITORSDIR . '/' . NV_EDITOR . '/nv.php';
        }

        $htmlcontent = htmlspecialchars(nv_editor_br2nl($data_block['htmlcontent']));
        

        $html = '';
         $html .= '<div class="form-group">';
             $html .= '<label class="control-label col-sm-6">Tiêu đề:</label>';
             $html .= '<div class="col-sm-18">';
                 $html .= '<input type="text" name="tieude" requied class="form-control" value="' . ($data_block['title_header'] != "" ? $data_block['title_header'] : "") . '"/></div>';
         $html .= '</div>';
         $html .= '<div class="form-group">';
             $html .= '<label class="control-label col-sm-6">Giới thiệu:</label>';
             $html .= '<div class="col-sm-18">';
                 $html .= '<input type="text" name="about" requied class="form-control" value="' . ($data_block['about'] != "" ? $data_block['about'] : "") . '"/></div>';
         $html .= '</div>';
         $html .= '<div class="form-group">';
             $html .= '<label class="control-label col-sm-6">Nội dung:</label>';
             $html .= '<div class="col-sm-18">';
             $htmlcontent = htmlspecialchars(nv_editor_br2nl($data_block['content_body']));
                if (defined('NV_EDITOR') and nv_function_exists('nv_aleditor')) {
                    $html .= nv_aleditor('content_body', '100%', '150px', $htmlcontent);
                } else {
                    $html .= '<textarea style="width: 100%" name="content_body" id="htmlcontent" cols="20" rows="8">' . $data_block['content_body'] . '</textarea>';
                }
         $html .= '</div>';
        return $html;
    }

    /**
     * nv_block_config_text_banner_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_content_header_submit($module, $lang_block)
    {
        global $nv_Request;

        $return                             = array();
        $return['error']                    = array();
        $return['config']                   = array();
        $return['config']['tieude']         = $nv_Request->get_title('tieude', 'post', '');
        $return['config']['about']          = $nv_Request->get_title('about', 'post', '');
        $htmlcontent = $nv_Request->get_editor('content_body', '', NV_ALLOWED_HTML_TAGS);
        $htmlcontent = strtr($htmlcontent, array(
            "\r\n" => '',
            "\r" => '',
            "\n" => ''
        ));
        $return['config']['content_body']    = $htmlcontent;
        return $return;
    }

    /**
     * nv_block_global_text_banner()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_global_content_header($block_config)
    {
        global $global_config;
        if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.content_header.tpl')) {
            $block_theme = $global_config['module_theme'];
        } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.content_header.tpl')) {
            $block_theme = $global_config['site_theme'];
        } else {
            $block_theme = 'default';
        }

        $xtpl = new XTemplate('global.content_header.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
        $xtpl->assign('TEMPLATE', $block_theme);
        $xtpl->assign('CONTENT', $block_config);

        $xtpl->parse('main');
        return $xtpl->text('main');
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_global_content_header($block_config);
}
