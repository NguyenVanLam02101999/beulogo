<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2018 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate March 16, 2018 11:34:27 AM
 */

if (! defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (! nv_function_exists('nv_block_about_home')) {
    /**
     * nv_block_config_about_home()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_about_home($module, $data_block, $lang_block)
    {
        global $db_config, $nv_Cache, $site_mods;
        
        $html = $block = '';

        $html .= '<div class="form-group">';
        $html .= '  <label class="control-label col-sm-6">' . $lang_block['module'] . ':</label>';
        $html .= '  <div class="col-sm-18"><select name="config_module_name" id="config_module_name" class="w200 form-control">';
        $sql = 'SELECT title, module_data, custom_title FROM ' . $db_config['prefix'] . '_' . NV_LANG_DATA . '_modules WHERE module_file = "page"';
        $list = $nv_Cache->db( $sql, 'title', $module );
        foreach( $list as $l )
        {
            $sel = ( $data_block['module_name'] == $l['title'] ) ? ' selected' : '';
            $html .= '<option value="' . $l['title'] . '" ' . $sel . '>' . $l['custom_title'] . '</option>';
            $block .= nv_get_blocklist_about_home( $l['title'] );
        }
        
        $html .= '  </select>';
        $html .= '  <div style="display: none">' . $block . '</div>';
        $html .= '  </div>';
        $html .= '</div>';
        
        $html .= '<script type="text/javascript">';
        $html .= '  $("#div-blockid").html( $("#select_" + $(\'#config_module_name\').val() ).html() ).find("select").attr("name", "config_posts");';
        $html .= '  $("#config_module_name").change(function() {';
        $html .= '      $("#div-blockid").html("<img src=\'' . NV_BASE_SITEURL . NV_ASSETS_DIR . '/images/load_bar.gif\' />").html( $("#select_" + $(this).val() ).html() ).find("select").attr("name", "config_posts");';
        $html .= '  });';
        $html .= '</script>';

        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['posts'] . ':</label>';
        $html .= '<div class="col-sm-18" id="div-blockid"></div>';
        $html .= '</div>';
        
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['bodytext_length'] . ':</label>';
        $html .= '<div class="col-sm-18"><input type="text" class="form-control w200" name="config_bodytext_length" size="5" value="' . $data_block['bodytext_length'] . '"/></div>';
        $html .= '</div>';

        return $html;
    }

    function nv_get_blocklist_about_home( $module )
    {
        global $site_mods, $data_block, $nv_Cache, $lang_block;

        $html = '';
        $html .= '<div id="select_' . $module . '">';
        $html .= '<select class="form-control w200">';
        $html .= '<option value="0">---' . $lang_block['posts_c'] . '---</option>';
        $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . ' ORDER BY weight ASC';
        $list = $nv_Cache->db($sql, '', $module);
        foreach ($list as $l) {
            $html .= '<option value="' . $l['id'] . '" ' . (($data_block['posts'] == $l['id']) ? ' selected="selected"' : '') . '>' . $l['title'] . '</option>';
        }
        $html .= '</select>';
        $html .= '</div>';

        return $html;
    }

    /**
     * nv_block_config_about_home_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_about_home_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['module_name'] = $nv_Request->get_title('config_module_name', 'post', 'about');
        $return['config']['posts'] = $nv_Request->get_int('config_posts', 'post', 0);
        $return['config']['bodytext_length'] = $nv_Request->get_int('config_bodytext_length', 'post', 20);
        return $return;
    }

    /**
     * nv_block_about_home()
     *
     * @return
     */
    function nv_block_about_home($block_config)
    {
        global $global_config, $site_mods, $module_config, $nv_Cache, $db, $lang_block, $module_name, $db_slave, $lang_block;

        $module = 'about';

        if (! isset($site_mods[$module])) {
            return '';
        }
        
        $is_show = false;
        
        $pattern = '/^' . NV_LANG_DATA . '\_([a-zA-z0-9\_\-]+)\_([0-9]+)\_' . NV_CACHE_PREFIX . '\.cache$/i';
        
        $cache_files = nv_scandir(NV_ROOTDIR . '/' . NV_CACHEDIR . '/' . $module, $pattern);
        
        if (($count = sizeof($cache_files)) >= 1) {
            $num = rand(1, $count);
            --$num;
            $cache_file = $cache_files[$num];
            
            if (($cache = $nv_Cache->getItem($module, $cache_file)) != false) {
                $cache = unserialize($cache);
                $link = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $cache['alias'] . $global_config['rewrite_exturl'];
                $title = $cache['page_title'];
                $description = strip_tags($cache['description']);
                $bodytext = strip_tags($cache['bodytext']);
                
                $is_show = true;
            }
        }
        if (! $is_show) {
            $sql = 'SELECT id, title, alias, image, imagealt, description, bodytext, keywords, add_time, edit_time FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . ' WHERE id = ' . $block_config['posts'];
            if (($query = $db_slave->query($sql)) !== false) {
                if (($row = $query->fetch()) !== false) {
                    if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.block_freecontent.tpl')) {
                        $block_theme = $global_config['module_theme'];
                    } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.block_freecontent.tpl')) {
                        $block_theme = $global_config['site_theme'];
                    }

                    if (! empty($row['image']) and file_exists(NV_ROOTDIR . NV_BASE_SITEURL . NV_ASSETS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $row['image'])) {
                        $images = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $row['image'];
                    } elseif (! empty($row['image'])) {
                        $images = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $row['image'];
                    }else{
                        $images = NV_BASE_SITEURL . '/themes/' . $global_config['site_theme'] . '/images/no_image_about.png';
                    }

                    $link = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $row['alias'] . $global_config['rewrite_exturl'];
                    $title = $row['title'];
    
                    if(!empty($row['description'])){                        
                        $description = strip_tags($row['description']);
                        $bodytext = nv_clean60($description, $block_config['bodytext_length']);
                    }else{
                        $bodytext = strip_tags($row['bodytext']);
                        $bodytext = nv_clean60($bodytext, $block_config['bodytext_length']);
                    }
                    $bodytext = nl2br($bodytext);
                    
                    $is_show = true;
                }
            }
        }
        
        if ($is_show) {
            if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.about_home.tpl')) {
                $block_theme = $global_config['module_theme'];
            } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.about_home.tpl')) {
                $block_theme = $global_config['site_theme'];
            } else {
                $block_theme = 'default';
            }
            
            $xtpl = new XTemplate('global.about_home.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
            
            $xtpl->assign('TEMPLATE', $block_theme);            
            $xtpl->assign('BLANG', $lang_block);
            $xtpl->assign('LINK', $link);
            $xtpl->assign('TITLE', $title);
            $xtpl->assign('IMAGES', $images);
            $xtpl->assign('BODYTEXT', $bodytext);
            
            $xtpl->parse('main');
            return $xtpl->text('main');
        }
        
        return '';
    }
}

if (defined('NV_SYSTEM')) {
    $content = nv_block_about_home($block_config);
}
