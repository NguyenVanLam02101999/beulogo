<!-- BEGIN: main  -->
	<div class="content_header">
		<span class="border_logic_header"></span>
		<h3 class="content_header_title">{CONTENT.tieude}</h3>
		<p class="content_header_about">{CONTENT.about}</p>
		<div class="content_header_body">
			{CONTENT.content_body}
		</div>
	</div>
<!-- END: main -->