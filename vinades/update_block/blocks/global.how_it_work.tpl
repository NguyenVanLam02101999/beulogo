<!-- BEGIN: main -->
<div class="wraper row content_how_it_word">
	<div class="content_how_it_word hidden-xs hidden-sm">
    	<table>
    		<tr>
    			<td width="50%">
    				<div class="txt"><h2 class="content_header_title"><a  href="{LINK}" class="hover_red" title="{TITLE}">{TITLE}</a></h2></div>
		        	<span class="border_logic_down"></span>
		            <div class="content_about_work_it">
		                <p class="bodytext">{BODYTEXT}</p>
		            </div>
    			</td>
    			<td>
    				 <a href="{LINK}" title="{TITLE}" class="hinhanh_how_work_it"><img alt="{TITLE}" src="{IMAGES}" /></a>
    			</td>
    		</tr>
    	</table>
    </div>
    <div class="content_how_it_word hidden-lg hidden-md">
        <div class="col-xs-24 col-sm-12 col-md-12 rotate">
            <a href="{LINK}" title="{TITLE}" class="hinhanh_how_work_it"><img alt="{TITLE}" src="{IMAGES}" /></a>
        </div>
        <div class="col-xs-24 col-sm-12 col-md-12 rotate" style="padding-left:0;">
            <div class="txt"><h2 class="content_header_title"><a  href="{LINK}" class="hover_red" title="{TITLE}">{TITLE}</a></h2></div>
        	<span class="border_logic_down"></span>
            <div class="content_about_work_it">
                <p class="bodytext">{BODYTEXT}</p>
            </div>
        </div>
    </div>
</div>
<!-- END: main -->
